<?php
$file = explode('.',$_GET['src']);
$ext = strtolower(end($file));
if(in_array($ext,array('jpg','jpeg','bmp','gif','png'))):
	if($ext == 'jpg') $ext = 'jpeg';
	header('Content-type: image/'.$ext);
	header('Content-Disposition: inline;');
elseif(in_array($ext,array('js'))):
	header("content-type: application/javascript");
endif;
echo file_get_contents('../'.$_GET['src']);