$(document).ready(function(){
	$("input[type=text].integer").maskMoney({symbol:'',decimal:'.',thousands: '',precision:0})
	$("input[type=text].decimal_2").maskMoney({symbol:'',decimal:'.',thousands: '',precision:2})
	$('input[type=text].decimal_3').maskMoney({symbol:'',decimal:'.',thousands: '',precision:3});
	$('input[type=text].integer').each(function(i){ if($(this).val() == '') $(this).val('0')	});
	$('input[type=text].decimal_2').each(function(i){ if($(this).val() == '') $(this).val('0.00')	});
	$('input[type=text].decimal_3').each(function(i){ if($(this).val() == '') $(this).val('0.000')	});
	
	if($('.Promocao input:checked').val() == 0) $('.fields_promocoes').hide();
	
	$('.Promocao input').change(function(){
		if($('.Promocao input:checked').val() == 0) { 
			$('.fields_promocoes').hide();
			$('.fields_promocoes input').attr("disabled","disabled"); 
		 } else { 
			$('.fields_promocoes').show();
			$('.fields_promocoes input').removeAttr("disabled"); 
		}
	});
	
	$( "input.date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "2012:1930" 
	});
	$('textarea.wysiwyg').redactor();
});