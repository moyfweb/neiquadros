<?php printf('<h1 class="title">Vendas</h1>');?>

<table class='grid_view autocall' fnc='GridView.zebra(this);'>
<tr class="legenda"><th>Data</th><th>Hora</th><th>Valor</th><th>Situa��o</th><th>Action</th></tr>
<?php 
foreach($lista as $i):
printf('
<tr class="grid"><td>%s</td><td>%s</td><td>R$ %s</td><td>%s</td><td>%s</td></tr>',
	CData::format('d/m/Y',$i->DataCadastro),
	CData::format('H:i:s',$i->DataCadastro),
	number_format($i->Total+$i->ValorFrete ,2,',','') ,
	CarrinhoSVenda::getSituacao($i->SituacaoVenda),
	tag::a(H::link('vendas','item',$i->IDCarrinho),'Visualizar','Visualizar','class="view"')
);
endforeach;

echo "
<tr> 
	<td class='pagination' colspan='5'>
	<ul>
	".$model->linkPrev()." 
	".$model->getCurrentPages()." 
	".$model->getNavigationGroupLinks()." 
	".$model->linkNext()." 
	</ul>
	</td>
</tr>";
?>
</table>