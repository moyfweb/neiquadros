<?php printf('<h1 class="title">Venda N� %s</h1>',$venda->IDCarrinho);?>

<table class='grid_view autocall' fnc='GridView.zebra(this);'>

<tr class="legenda"><th>Produto</th><th>Pre�o</th><th>Unidades</th><th>Subtotal</th></tr>
<?php 
foreach($itens as $i):
$P = $i->produto();
printf('
<tr class="grid"><td>%s</td><td>R$ %s</td><td>%s</td><td>R$ %s</td></tr>',
	$P->Nome,
	number_format($i->Preco ,2,',','') ,
	$i->Unidades,
	number_format($i->Preco*$i->Unidades ,2,',','')
);
endforeach;	
printf('
		<tr class="soma">
			<td colspan="4">Subtotal: R$ %s</td>
		</tr>
		<tr class="soma">
			<td colspan="4">Frete: R$ %s</td>
		</tr>
		<tr class="soma">
			<td colspan="4">Total: R$ %s</td>
		</tr>',
		number_format($venda->Total,2,',',''),
		number_format($venda->ValorFrete,2,',',''),
		number_format($venda->Total + $venda->ValorFrete,2,',','')
		);
?>
</table>


<div>
</div>