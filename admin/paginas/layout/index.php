<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-br">
<head>
<?php include("paginas/head.php"); ?>
</head>
<body>
<div id='site'><!-- SITE -->


<div id='topo'><!-- TOPO -->
	<div class='contato'>
	Login Info!
	</div>
</div><!-- TOPO FIM -->

<div id='meio'><!-- MEIO -->
	<ul class='menu'>
		<?php 
		if(CLogin::id() > 0):
			echo '<li>'.tag::a(H::link('usuario'),'Usuarios',true).'</li>';
			echo '<li>'.tag::a(H::link('cliente'),'Clientes',true).'</li>';
			echo '<li>'.tag::a(H::link('file','index'),'Mapear Fotos',true).'</li>';
			echo '<li>'.tag::a(H::link('vendas'),'Vendas',true).'</li>';
			echo '<hr/>';
			echo '<li  class="logout">'.tag::a(H::link('login','logout'),'Logout',true).'</li>';
		else: 
			echo '<li>'.tag::a(H::link('login','index'),'Login',true).'</li>';
		endif;		
		?>
	</ul>
	<div class='content'><!-- CONTEUDO -->
		<?php if(isset($menu_options)) include(H::path().$menu_options); //pagina onde fica o conteudo ?> 
		<div class='content-in'>
			<div class='content-margin'>
			<?php include(H::path().H::file()); //pagina onde fica o conteudo ?>  
			</div>
		</div>
	</div><!-- CONTEUDO FIM -->
	<br style='clear: both;'/>
</div><!-- MEIO FIM -->


</div><!-- SITE FIM-->
</body>
</html>