<div class='lista-prd' id="div_album">
<?php 
if(!isset($_GET['location'])) $_GET['location'] = '';
$parent_folders = explode('/',substr($_GET['location'],0,-1));
array_pop($parent_folders);
$up = implode('/',$parent_folders).'/';
if($up == '/') $up = '';
$count = 0;
if(!empty($_GET['location'])):
	printf('
	<div>
		<a href="%1$s" title="Voltar" class="parent">
			VOLTAR
		</a>
	</div>','?location='.$up,'Voltar');
endif;

foreach($files as $f):
	if($f->Tipo == 1):
		printf('
			<div class="box">
				<p><a href="%1$s" title="%2$s" class="dir">%2$s</a></p>
				<a href="%1$s" title="%2$s" class="thumb folder">
					&nbsp;
				</a>
			</div>
		',
		H::link('file','index','?location='.$f->Folder.$f->File),
		$f->Name
		);
	else:
	printf('
		<div class="box">
			<a href="%3$s" title="%1$s" class="thumb fancybox-buttons" data-fancybox-group="%5s" style="background-image: url(\'%4$s\')">&nbsp;</a>
			<p>%1$s</p>
			<div class="clear"></div>
		</div>
	',
	$f->Name,
	$f->Folder,
	$f->web(),
	$f->thumb(),
	'GP_'.$count
	);
	endif;
	if($count%4==3):
		#echo '<div class="clear"></div>';
	endif;
	$count += 1;
endforeach;
echo '<div class="clear"></div>';
/*
printf('
<div class="pagination"><ul>%s %s %s %s</ul></div>',
	$model->linkPrev(),
	$model->getCurrentPages(),
	$model->getNavigationGroupLinks(),
	$model->linkNext()
	);
*/
?>
	<div class="clear"></div>
</div>
