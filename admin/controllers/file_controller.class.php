<?php
class file_controller
{
	public static function INIT() { 
		#if(USR::type() != 2) H::redirect('central','site','index');
		H::path('paginas/file/');
		
		H::css(array(
			'index.css',
			'jquery.fancybox.css',
			'jquery.fancybox-buttons.css',
			'grid.css',
			'produto.css'));
		H::js(array(
			'jquery/jquery.fancybox.js',
			'jquery/jquery.fancybox.pack.js',
			'jquery/jquery.fancybox-buttons.js',
			'global/GridView.js',
			'index.js'));
		H::vars(array('menu_options'=>'menu.php'));
		if(!CLogin::id()) return false; else return true;
	}
	
	public static function render() { 
		H::render('paginas/layout/index.php');
	}
	
	public static function index() {
		File::map();
		$vars = new stdClass();
		$model = new File();
		$model->IDParent = File::parentID();
		$model->setOrders('Tipo ASC, Name ASC');
		$vars->files = $model->findAll();
		H::vars($vars);
		H::config('lista.php','Galeria');
		H::css(array(
			'produtos.css'));
		H::js(array(
			'file.js'));
		self::render();
	
	}
	
	public static function mapp() {
		$vars = new stdClass();
		$model = new File();
		$model->IDParent = File::parentID();
		$vars->files = $model->findAll();
		H::vars($vars);
		H::config('lista.php','Galeria');
		self::render();
	
	}
	
}