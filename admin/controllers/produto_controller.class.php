<?php
class produto_controller{

	public static function INIT() { 
		#if(USR::type() != 2) H::redirect('central','site','index');
		H::path('paginas/produto/');
		
		H::css(array(
			'index.css',
			'jquery.fancybox.css',
			'jquery.fancybox-buttons.css',
			'grid.css',
			'produto.css'));
		H::js(array(
			'jquery/jquery.fancybox.js',
			'jquery/jquery.fancybox.pack.js',
			'jquery/jquery.fancybox-buttons.js',
			'global/GridView.js',
			'index.js'));
		H::vars(array('menu_options'=>'menu.php'));
		if(!CLogin::id()) return false; else return true;
	}
	
	public static function render() { 
		H::render('paginas/layout/index.php');
	}
	
	
	public static function index() { self::listing(); }
	
	public static function listing() {
		$vars = new stdClass();
		$model = new ProdutoFotoQ();
		$model->request('Produto');
		$model->bQuery();
		$model->setPagination(100);
		$vars->produtos = $model->findAll();
		$vars->model = $model;
		H::vars($vars);
		H::config('listing.php','Produtos');
		self::render();
	}
	
	public static function teste() { 
		
		$res42 = isset($_POST['teste']) ? $_POST['teste'] : uniqid();
		echo "<div class='autocall' fnc=\"alert('hehehehe')\">$res42</div>";
	}
	
	public static function create() { self::form('Novo Produto');}
	public static function edit() {	self::form('Editar Produto',H::cod());}
	private static function form($titulo,$id = null) {	
		$vars = new stdClass();
		$errors = array();
		$model = new Produto();
		$promo = new ProdutoPromo();
		if(isset($_POST['Produto'])):
			$model->request('Produto');
			if($id != null): 
				$model->IDProduto = $id;
				$model->DataAtualizacao = date('Y-m-d H:i:s');
			else:
				$model->DataCriacao = date('Y-m-d H:i:s');
				$model->DataAtualizacao = $model->DataCriacao;
			endif;
			
			$errors = Validate::model($model)->errors;
			
			#echo '<pre>';var_dump($errors);die;
			if($model->Status === null) $model->Status=0;
			if($model->IDCategoria === null) $model->IDCategoria=0;
			
			if(count($errors) < 1):
				if(!$data = $model->save()) die('N�o foi possivel salvar');
				else H::redirect('produto','view',$data->IDProduto);
			else:
			endif;
			
		else:
			if($id):
				$model->IDProduto = $id;
				$model = $model->findOneData();
			endif;
		endif;
		$vars->tit = H::cod() == null ? $titulo : $titulo." : '".$model->Nome."'";
		$vars->model = $model;
		$vars->promo = $promo;
		$vars->errors = $errors;
		H::config('form.php',$vars->tit);
		H::css(array(
			'smoothness/jquery-ui.css',
			'redactor/redactor.css',
			));
		H::js(array(
			'jquery/jquery.validate.js',
			'jquery/jquery.metadata.js',
			'jquery/jquery.ui.core.js',
			'jquery/jquery.ui.widget.js',
			'jquery/jquery.ui.datepicker.js',
			'jquery/jquery.ui.datepicker-pt-BR.js',
			'jquery/jquery.mask.js',
			'jquery/jquery.maskMoney.js',
			'redactor.js',
			'produto.js'));
		H::vars($vars);
		self::render();

	}
	
	public static function view() {
		$vars = new stdClass();
		$model = new Produto();
		$model->IDProduto = H::cod();
		$vars->data = $model->findOne();
		H::config('view.php','Produto: '.$vars->data->Nome);
		H::vars($vars);
		self::render();
	}
	
	public static function delete() {
		$model = new Produto();
		$model->IDProduto = H::cod();
		if(!$model->remove()) die('N�o foi possivel excluir');
		H::redirect('produto','index');
	}
	
	public static function foto_pop_up(){
		$model = new Foto();
		$model->IDFoto = URL::friend(3);
		if(!empty($model->IDFoto)) $model = $model->findOne();
		
		H::vars(array('data'=>$model));
		H::render('paginas/upload/form.php');
	}
	
	public static function fotos() {
		$vars = new stdClass();
		$model = new ProdutoFotoR();
		$model->IDProduto = H::cod();
		$model->setPagination();
		$vars->fotos = $model->findAll();
		$vars->model = $model;
		H::js(array('foto.js'));
		H::vars($vars);
		H::config('fotos.php','');
		self::render();
	}
	
	public static function foto_rm(){
		$model = new ProdutoFoto();
		$model->ID = URL::friend(3);
		if(!$model->remove()) die('N�o foi possivel excluir');
		H::redirect('produto','fotos',H::cod());
	}
	
	public static function upload() {
		if(ProdutoFoto::requestSave('foto',H::cod())) H::redirect('produto','fotos',H::cod());
		
	}
	
	public static function categorias() {
		$vars = new stdClass();
		$vars->errors = array();
		$model = new ProdutoCategoria();
		if(isset($_GET['Categoria'])) $model->request('Categoria');
		#$model->setPagination();
		$vars->categorias = $model->findAll();
		$vars->model = $model;
		H::vars($vars);
		if(isset($_GET['update'])):
			header ('Content-type: text/html; charset=ISO-8859-1');		
			H::render(H::path().'categorias.php');
		else:
			H::config('categorias.php','Categorias de Produtos');
			self::render();
		endif;
	}
	
	public static function categorias_form() {
		$vars = new stdClass();
		$vars->errors = array();
		$model = new ProdutoCategoria();
		$cod = H::cod();
		if(!empty($cod)):
			$model->IDCategoria = H::cod();
			$model = $model->findOne();
		endif;
		if(isset($_POST['ProdutoCategoria'])):
			$model->request('ProdutoCategoria');
			if($model->Status === null) $model->Status=0;
			$vars->errors = Validate::model($model)->errors;
			$model->Categoria = utf8_decode($model->Categoria);
			$model->Descricao = utf8_decode($model->Descricao);
			if(count($vars->errors) > 0):  
			elseif(!$data = $model->save()): die('N�o foi possivel salvar');
			else: return header("Location: ".H::link('produto','categorias').'?update=true');
			endif;
		endif;
		$vars->model = $model;
		H::vars($vars);
		header ('Content-type: text/html; charset=ISO-8859-1');
		H::render(H::path().'categorias_form.php');
	}
	
	public static function categorias_confirm() { 
		header ('Content-type: text/html; charset=ISO-8859-1');
		$model = new ProdutoCategoria();
		$model->IDCategoria = H::cod();
		$C = $model->findOne();
		H::vars(array('C'=>$C));
		H::render(H::path().'categorias_confirm.php');
	}
	
	
	public static function categorias_delete(){
		$model = new ProdutoCategoria();
		$model->IDCategoria = H::cod();
		if(!$model->remove()) die('N�o foi possivel excluir');
		header("Location: ".H::link('produto','categorias').'?update=true');
	}
	
	public static function import_mytrius() { 
		H::render(H::path().'import-mytrius.php');
		
	}
}