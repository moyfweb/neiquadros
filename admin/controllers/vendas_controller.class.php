<?php
class vendas_controller{

	public static function INIT() { 
		#if(USR::type() != 2) H::redirect('central','site','index');
		H::path('paginas/vendas/');
		
		H::css(array(
			'index.css',
			'jquery.fancybox.css',
			'jquery.fancybox-buttons.css',
			'grid.css',
			'produto.css'));
		H::js(array(
			'jquery/jquery.fancybox.js',
			'jquery/jquery.fancybox.pack.js',
			'jquery/jquery.fancybox-buttons.js',
			'GridView.js',
			'index.js'));
		H::vars(array('menu_options'=>'menu.php'));
		if(!CLogin::id()) return false; else return true;
	}
		
	public static function render() { 
		H::render('paginas/layout/index.php');
	}
	
	public static function index() { 
		
		$vars = new stdClass();
		$model = new Carrinho();
		$model->setPagination();
		$model->setOrders(array('SituacaoVenda ASC', 'DataCadastro DESC'));
		$vars->lista = $model->findAll();
		$vars->model = $model;
		H::vars($vars);
		H::config('listing.php','Vendas');
		self::render();
	}
	
	public static function item() { 
		$vars = new stdClass();
		$model = new Carrinho();
		$model->IDCarrinho = H::cod();
		$vars->venda = $model->findOne();
		$model = new CarrinhoItem();
		$model->IDSCarrinho = $vars->venda->IDSCarrinho;
		$vars->itens = $model->findAll();
		H::css(array('vendas.css'));
		H::vars($vars);
		H::config('item.php','Venda N�.'.$vars->venda->IDCarrinho);
		self::render();
		
	}
}
	