CREATE DATABASE  IF NOT EXISTS `nei_foto_producoes` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `nei_foto_producoes`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: nei_foto_producoes
-- ------------------------------------------------------
-- Server version	5.5.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ecom_carrinho`
--

DROP TABLE IF EXISTS `ecom_carrinho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_carrinho` (
  `IDCarrinho` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `IDSCarrinho` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `IDCliente` int(11) unsigned NOT NULL,
  `CodFrete` int(10) unsigned NOT NULL,
  `Total` decimal(10,2) NOT NULL,
  `Volumes` int(11) unsigned NOT NULL,
  `ValorFrete` decimal(10,2) NOT NULL,
  `DataCadastro` datetime NOT NULL,
  `SituacaoVenda` int(3) unsigned NOT NULL DEFAULT '1',
  `SituacaoEntrega` int(3) unsigned NOT NULL DEFAULT '1',
  `FormaPagamento` int(3) unsigned NOT NULL,
  `EnderecoEntrega` int(11) unsigned NOT NULL,
  `EmailCobranca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MP` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Deposito` int(1) unsigned NOT NULL DEFAULT '0',
  `Observacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IDCarrinho`),
  UNIQUE KEY `IDSCarrinho` (`IDSCarrinho`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_carrinho`
--

LOCK TABLES `ecom_carrinho` WRITE;
/*!40000 ALTER TABLE `ecom_carrinho` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_carrinho` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_carrinho_deposito`
--

DROP TABLE IF EXISTS `ecom_carrinho_deposito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_carrinho_deposito` (
  `IDDeposito` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDCarrinho` int(11) unsigned NOT NULL,
  `IDCliente` int(11) unsigned NOT NULL,
  `Numero` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Data` date NOT NULL,
  `Hora` time NOT NULL,
  `Observacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IDDeposito`),
  UNIQUE KEY `IDCarrinho` (`IDCarrinho`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_carrinho_deposito`
--

LOCK TABLES `ecom_carrinho_deposito` WRITE;
/*!40000 ALTER TABLE `ecom_carrinho_deposito` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_carrinho_deposito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_carrinho_forma_pagamento`
--

DROP TABLE IF EXISTS `ecom_carrinho_forma_pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_carrinho_forma_pagamento` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_carrinho_forma_pagamento`
--

LOCK TABLES `ecom_carrinho_forma_pagamento` WRITE;
/*!40000 ALTER TABLE `ecom_carrinho_forma_pagamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_carrinho_forma_pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_carrinho_item`
--

DROP TABLE IF EXISTS `ecom_carrinho_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_carrinho_item` (
  `ID` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `IDSCarrinho` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `IDProduto` int(11) unsigned NOT NULL,
  `Unidades` int(11) unsigned NOT NULL,
  `Preco` float(11,2) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `IDSCarrinho` (`IDSCarrinho`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_carrinho_item`
--

LOCK TABLES `ecom_carrinho_item` WRITE;
/*!40000 ALTER TABLE `ecom_carrinho_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_carrinho_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_carrinho_sit_entrega`
--

DROP TABLE IF EXISTS `ecom_carrinho_sit_entrega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_carrinho_sit_entrega` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Situacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_carrinho_sit_entrega`
--

LOCK TABLES `ecom_carrinho_sit_entrega` WRITE;
/*!40000 ALTER TABLE `ecom_carrinho_sit_entrega` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_carrinho_sit_entrega` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_carrinho_sit_venda`
--

DROP TABLE IF EXISTS `ecom_carrinho_sit_venda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_carrinho_sit_venda` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Situacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_carrinho_sit_venda`
--

LOCK TABLES `ecom_carrinho_sit_venda` WRITE;
/*!40000 ALTER TABLE `ecom_carrinho_sit_venda` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_carrinho_sit_venda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_cliente`
--

DROP TABLE IF EXISTS `ecom_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_cliente` (
  `IDCliente` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Senha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Recuperacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`IDCliente`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_cliente`
--

LOCK TABLES `ecom_cliente` WRITE;
/*!40000 ALTER TABLE `ecom_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_cliente_contato`
--

DROP TABLE IF EXISTS `ecom_cliente_contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_cliente_contato` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDCliente` int(11) unsigned NOT NULL,
  `IDContato` bigint(19) unsigned NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_cliente_contato`
--

LOCK TABLES `ecom_cliente_contato` WRITE;
/*!40000 ALTER TABLE `ecom_cliente_contato` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_cliente_contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_cliente_endereco`
--

DROP TABLE IF EXISTS `ecom_cliente_endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_cliente_endereco` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDCliente` int(11) unsigned NOT NULL,
  `IDEndereco` int(11) unsigned NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_cliente_endereco`
--

LOCK TABLES `ecom_cliente_endereco` WRITE;
/*!40000 ALTER TABLE `ecom_cliente_endereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_cliente_endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_cliente_foto`
--

DROP TABLE IF EXISTS `ecom_cliente_foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_cliente_foto` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDCliente` int(11) unsigned NOT NULL,
  `IDFoto` bigint(19) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_cliente_foto`
--

LOCK TABLES `ecom_cliente_foto` WRITE;
/*!40000 ALTER TABLE `ecom_cliente_foto` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_cliente_foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_compra`
--

DROP TABLE IF EXISTS `ecom_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_compra` (
  `IDCompra` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDCliente` int(11) unsigned NOT NULL,
  `IDProduto` int(11) unsigned NOT NULL,
  `IDCarrinho` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Data` date NOT NULL,
  `Quantidade` decimal(10,3) NOT NULL,
  `Ativo` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`IDCompra`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_compra`
--

LOCK TABLES `ecom_compra` WRITE;
/*!40000 ALTER TABLE `ecom_compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_contato`
--

DROP TABLE IF EXISTS `ecom_contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_contato` (
  `IDContato` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDTipo` int(10) unsigned NOT NULL,
  `Valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IDContato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_contato`
--

LOCK TABLES `ecom_contato` WRITE;
/*!40000 ALTER TABLE `ecom_contato` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_contato_tipo`
--

DROP TABLE IF EXISTS `ecom_contato_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_contato_tipo` (
  `IDTipo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IDTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_contato_tipo`
--

LOCK TABLES `ecom_contato_tipo` WRITE;
/*!40000 ALTER TABLE `ecom_contato_tipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_contato_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_endereco`
--

DROP TABLE IF EXISTS `ecom_endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_endereco` (
  `IDEndereco` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Pais` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Brasil',
  `Estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Bairro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Logadouro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Numero` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Complemento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CEP` int(8) NOT NULL,
  `Info` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`IDEndereco`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_endereco`
--

LOCK TABLES `ecom_endereco` WRITE;
/*!40000 ALTER TABLE `ecom_endereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_foto`
--

DROP TABLE IF EXISTS `ecom_foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_foto` (
  `IDFoto` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `Arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Tamanho` decimal(10,3) NOT NULL,
  `Nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IDFoto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_foto`
--

LOCK TABLES `ecom_foto` WRITE;
/*!40000 ALTER TABLE `ecom_foto` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_produto`
--

DROP TABLE IF EXISTS `ecom_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_produto` (
  `IDProduto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDCategoria` int(11) unsigned NOT NULL DEFAULT '0',
  `IDMatrix` int(11) unsigned NOT NULL DEFAULT '0',
  `DataCadastro` datetime NOT NULL DEFAULT '2013-12-04 00:00:00',
  `DataAtualizacao` datetime NOT NULL DEFAULT '2013-12-04 00:00:00',
  `Nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `REF` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Estoque` decimal(10,3) NOT NULL DEFAULT '0.000',
  `Resumo` text COLLATE utf8_unicode_ci,
  `Descricao` text COLLATE utf8_unicode_ci,
  `Especificacoes` text COLLATE utf8_unicode_ci,
  `Extra` text COLLATE utf8_unicode_ci,
  `Preco` decimal(10,2) NOT NULL,
  `PrecoCaixa` decimal(10,2) NOT NULL,
  `ItensCaixa` int(5) NOT NULL,
  `Validade` date DEFAULT NULL,
  `Peso` decimal(10,3) DEFAULT NULL,
  `Altura` decimal(10,2) DEFAULT NULL,
  `Largura` decimal(10,2) DEFAULT NULL,
  `Comprimento` decimal(10,2) DEFAULT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IDProduto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_produto`
--

LOCK TABLES `ecom_produto` WRITE;
/*!40000 ALTER TABLE `ecom_produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_produto_categoria`
--

DROP TABLE IF EXISTS `ecom_produto_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_produto_categoria` (
  `IDCategoria` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `Categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `URLAmigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IDCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_produto_categoria`
--

LOCK TABLES `ecom_produto_categoria` WRITE;
/*!40000 ALTER TABLE `ecom_produto_categoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_produto_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_produto_foto`
--

DROP TABLE IF EXISTS `ecom_produto_foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_produto_foto` (
  `ID` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `IDProduto` int(11) unsigned NOT NULL,
  `IDFoto` bigint(19) unsigned NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_produto_foto`
--

LOCK TABLES `ecom_produto_foto` WRITE;
/*!40000 ALTER TABLE `ecom_produto_foto` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_produto_foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_produto_promocao`
--

DROP TABLE IF EXISTS `ecom_produto_promocao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_produto_promocao` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDProduto` int(11) unsigned NOT NULL,
  `Preco` decimal(10,2) NOT NULL,
  `PromoInicio` date NOT NULL,
  `PromoFim` date NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IDProduto` (`IDProduto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_produto_promocao`
--

LOCK TABLES `ecom_produto_promocao` WRITE;
/*!40000 ALTER TABLE `ecom_produto_promocao` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_produto_promocao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_usuario`
--

DROP TABLE IF EXISTS `ecom_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_usuario` (
  `IDUsuario` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDTipo` int(11) unsigned NOT NULL,
  `Nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Senha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`IDUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_usuario`
--

LOCK TABLES `ecom_usuario` WRITE;
/*!40000 ALTER TABLE `ecom_usuario` DISABLE KEYS */;
INSERT INTO `ecom_usuario` VALUES (1,1,'Moysés Oliveira','moyfweb@gmail.com','1f3a8471c55ef8be99c8791760e0dab3',1),(3,3,'novo','novo@novo.com','6b0aad3ed14f037654be19a3576801ab',-1),(4,3,'Outro','outro@outro.com.br','8832364f9f5eee5ec0f24e405b52a38b',1);
/*!40000 ALTER TABLE `ecom_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_usuario_contato`
--

DROP TABLE IF EXISTS `ecom_usuario_contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_usuario_contato` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDUsuario` int(11) unsigned NOT NULL,
  `IDContato` bigint(19) unsigned NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_usuario_contato`
--

LOCK TABLES `ecom_usuario_contato` WRITE;
/*!40000 ALTER TABLE `ecom_usuario_contato` DISABLE KEYS */;
INSERT INTO `ecom_usuario_contato` VALUES (1,1,1,-1),(2,1,2,1),(3,1,3,1),(4,3,4,1),(5,1,2,1),(6,1,3,1),(7,1,8,-1);
/*!40000 ALTER TABLE `ecom_usuario_contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_usuario_endereco`
--

DROP TABLE IF EXISTS `ecom_usuario_endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_usuario_endereco` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDUsuario` int(11) unsigned NOT NULL,
  `IDEndereco` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_usuario_endereco`
--

LOCK TABLES `ecom_usuario_endereco` WRITE;
/*!40000 ALTER TABLE `ecom_usuario_endereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `ecom_usuario_endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecom_usuario_tipo`
--

DROP TABLE IF EXISTS `ecom_usuario_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecom_usuario_tipo` (
  `IDTipo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IDTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecom_usuario_tipo`
--

LOCK TABLES `ecom_usuario_tipo` WRITE;
/*!40000 ALTER TABLE `ecom_usuario_tipo` DISABLE KEYS */;
INSERT INTO `ecom_usuario_tipo` VALUES (1,'Moderador'),(2,'Administrador'),(3,'Custom');
/*!40000 ALTER TABLE `ecom_usuario_tipo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-13 18:07:57
