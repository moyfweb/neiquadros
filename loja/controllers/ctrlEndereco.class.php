<?php
class ctrlEndereco {

	public static function INIT() {
		H::path('paginas/endereco/');
		H::acao(URL::friend(1));
		H::cod(URL::friend(2));
		H::js(array('jquery/jquery.mask.js','endereco.js'));
		
		if(in_array(H::acao(),array('selecionar')) && CLogin::id() == null):
			ctrlLogin::mkLogin(URL::friend(0).'/'.URL::friend(1));
			return true;
		endif;
			
		if(method_exists(get_class(),H::acao())):
			call_user_func(get_class()."::".H::acao());
		endif;
		return true;
	}
	
	private static function render() { 
		if(!isset($_GET['modal'])):
			H::render('paginas/layout/index.php'); 
		else:
			header('Content-type: text/html; charset=ISO-8859-1');
			H::render(H::path().H::file());
		endif;
	}
	
	public static function create() { self::form(true); }
	public static function edit() { 
		if(H::cod() == null) die('Sem ID');
		else self::form(false);
	}
	
	private static function form($create) { 
	
		$model = new ClienteEnderecoR();
		$model->ID = !$create ? H::cod() : null;
		if(!empty($model->ID)) $model = $model->findOne();
		if($create) $model->IDCliente = H::cod();
		if(CLogin::id() == null) die('Permiss�o negada');
		if(isset($_POST['ClienteEndereco'])):
			ClienteEndereco::savePost('ClienteEndereco',$model->IDCliente);
			H::redirect($_GET['act']);
		endif;
		H::vars(array('model'=>$model));
		H::config('form.php',(H::cod() == null ? 'Criar Endere�o' : 'Editar Endere�o'));
		self::render();
	}
		
	public static function selecionar() { 
		H::config('list.php','Endere�os');
		H::vars(array('tit'=>"Endere�o para Entrega"));
		self::render();
	}
	
	public static function remove() { 
		
		$model = new ClienteEndereco();
		$model->ID = H::cod();
		$model->IDCliente = CLogin::id();
		if(!$model->remove()) die('N�o foi possivel excluir');
		H::redirect($_GET['act']);
	}
	
	public static function busca_cep() {
		$endereco = correio::endereco(URL::friend(2));
		echo json_encode($endereco);
	}
}