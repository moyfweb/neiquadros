<?php
class ctrlUsuario {

	public static function INIT() {
		H::path('paginas/usuario/');
		$acao = current(explode('.html',URL::friend(1)));
		$cod = current(explode('.html',URL::friend(2)));
		H::acao($acao);
		H::cod($cod);
		H::css(array('usuario.css'));
		
		if(method_exists(get_class(),H::acao())):
			$actions = array('cadastro');
			$permissao = in_array(H::acao(),$actions);
			
			if(CLogin::id() == null && !$permissao): ctrlLogin::mkLogin('?url='.URL::atual());
			else: call_user_func(get_class()."::".H::acao());
			endif;
			
			return true;
		else: return false;
		endif;
	}

	private static function render() {
		if(!isset($_GET['modal'])):
			H::render('paginas/layout/index.php'); 
		else:
			header('Content-type: text/html; charset=ISO-8859-1');
			H::render(H::path().H::file());
		endif;
	}
		
	public static function index() { 
		$vars = new stdClass();
		$model = new Cliente();
		$model->IDCliente = CLogin::id();
		$vars->user = $model->findOne();
		$model = new ClienteContatoR();
		$model->IDCliente = CLogin::id();
		$vars->contatos = $model->findAll();
		$model = new Carrinho();
		$model->IDCliente = CLogin::id();
		$vars->lista = $model->findAll();
		$model = new Produto();
		$model->setLimit(3);
		$vars->favoritos = $model->findAll();
		$vars->tit = 'Endere�os para Entrega';
		H::vars($vars);
		H::css(array('endereco.css'));
		H::js(array('usuario.js','endereco.js'));
		H::config('index.php');
		self::render();
	}
	
	public static function info() { 
		$vars = new stdClass();
		$model = new Cliente();
		$model->IDCliente = CLogin::id();
		$vars->user = $model->findOne();
		$model = new ClienteContatoR();
		$model->IDCliente = CLogin::id();
		$vars->contatos = $model->findAll();
		$model = new Carrinho();
		$model->IDCliente = CLogin::id();
		$vars->lista = $model->findAll();
		$model = new Produto();
		$model->setLimit(3);
		$vars->favoritos = $model->findAll();
		$vars->tit = 'Endere�os para Entrega';
		H::vars($vars);
		H::config('info.php');
		H::css(array('endereco.css'));
		H::js(array('usuario.js','endereco.js'));
		self::render();
	}
	
	public static function compras() { 
		$vars = new stdClass();
		$model = new Carrinho();
		$model->IDCliente = CLogin::id();
		$vars->lista = $model->findAll();
		H::vars($vars);
		H::config('compras.php');
		self::render();
	}
	
	public static function compra() { 
		$vars = new stdClass();
		$model = new Carrinho();
		$model->IDCarrinho = H::cod();
		$vars->data = $model->findOneUpdate();
		$model = new CarrinhoItem();
		$model->IDSCarrinho = $vars->data->IDSCarrinho;
		$vars->itens = $model->findAll();
		$vars->info = CarrinhoSession::basicInfoCarrinho($vars->data->IDSCarrinho);
		H::vars($vars);
		H::css(array('endereco.css'));
		H::config('compra.php','Minha Compra em: '.date('d.m.Y'));
		self::render();
	}
	
	public static function pagamento() { 
		$url = self::botaoDePagamento(URL::friend(3));
		header('Location: '.$url);
	}
	
	public static function cadastro() { self::form('Novo Usu�rio');}
	
	public static function alterar() {	
		H::js(array('usuario.js'));
		self::form('Altera��o de Senha',CLogin::id());
	}
	
	private static function form($titulo,$id = null) {	
		$vars = new stdClass();
		$errors = array();
		$model = new Cliente();
		if(isset($_POST['Cliente'])):
			$model->request('Cliente');
			if(!empty($id)) $model->IDCliente = $id;
			else $model->Status = 2;
			
			$errors = Validate::model($model)->errors;
			
			if(!empty($model->Email) && $id === null):
				$temp = new Cliente();
				$temp->Email = $model->Email;
				if($temp->total() > 0):
					$errors['Email'] = 'Este e-mail j� est� cadastrado.';
				endif;
			endif;
			
			if(empty($model->Senha) && $id == null) $errors['Senha'] = 'O campo Senha � obrigatorio.';
			
			if(!empty($model->Senha)):
				if(strlen($model->Senha) < 6):
					$errors['Senha'] = 'O campo Senha deve conter mais que 6 caracteres.';
				elseif($model->Senha !=  $_POST['repetir_senha']):
					$errors['Senha'] = 'Os campos de Senhas n�o conferem (s�o diferentes).';
				else :
					$model->Senha = md5($model->Senha);
				endif;
			else: 
				$model->Senha = null;	
			endif;
			
			if(count($errors) < 1):
				SQLB::begin();
				if(!($data = $model->save())): die('Erro ao salvar');
				else:
					if(isset($_POST['ClienteContato'])):
						ClienteContato::saveList('ClienteContato',$data->IDCliente);
					endif;
					if(isset($_POST['ClienteEndereco'])):
						ClienteEndereco::saveList('ClienteEndereco',$data->IDCliente);
					endif;
					
					SQLB::commit();
					if($id == null):
						header('Location: '.$_SESSION['voltar_para_url']);
						die();
					endif;
					# SEM ERROS E SALVO COM SUCESSO
					H::redirect('login');
				endif;
			endif;
		else:
			if(!empty($id)):
				$model->IDCliente = $id;
				$model = $model->findOne();
			endif;
		endif;
		foreach(array('model','errors') as $v) $vars->{$v} = $$v;
		$vars->tit = H::cod() == null ? $titulo : $titulo." : '".$model->Nome."'";
		H::config('form.php',$vars->tit);
		H::css(array('cadastro.css'));
		H::js(array('jquery/jquery.validate.js',
			'jquery/jquery.metadata.js',
			'jquery/jquery.ui.core.js',
			'jquery/jquery.ui.widget.js',
			'jquery/jquery.ui.datepicker.js',
			'jquery/jquery.ui.datepicker-pt-BR.js',
			'jquery/jquery.mask.js',
			'jquery/jquery.maskMoney.js',
			'endereco.js')
			);
		
		if($id == null) $vars->hideLogin = true;
		
		H::vars($vars);
		#var_dump($_SESSION['voltar_para_url']);
		self::render();
	}
	
	
	public static function endereco() {
		H::path('paginas/endereco/');
		H::submenu('paginas/usuario/menu.php');
		H::config('list.php','Endere�os');
		self::render();
	}
	
	public static function view() {
		$vars = new stdClass();
		$model = new Cliente();
		$model->IDCliente = CLogin::id();
		$vars->data = $model->findOne();
		H::config('view.php','Cliente: '.$vars->data->Nome);
		H::vars($vars);
		self::render();
	}
	
	public static function deposito() {
		$vars = new stdClass();
		$model = new Carrinho();
		$model->IDCarrinho = H::cod();
		$data = $model->findOne();
		$model = new CarrinhoItem();
		$model->IDSCarrinho = $data->IDSCarrinho;
		$vars->itens = $model->findAll();
		$vars->errors = array();
		$model = new Deposito();
		$vars->salvo_deposito = Deposito::Depositado(H::cod());;
		$vars->sel_deposito = false;
		if(isset($_POST['Deposito'])):
			$model->request('Deposito');
			$model->IDCarrinho = H::cod();
			$model->IDCliente = CLogin::id();
			$vars->errors = Validate::model($model)->errors;
			$model->Data = CData::setDateBr($model->Data);
			var_dump($vars->errors);
			if(count($vars->errors) < 1):
				if(!($dep = $model->save())): die('Erro ao salvar');
				else: $vars->salvo_deposito = true;	endif;
			endif;
		endif;
		if((int)$data->Deposito == 0):
			Deposito::novaSolicitacao(H::cod(),CLogin::id());
			$vars->sel_deposito = true;
		endif;
			
		$vars->model = $model;
		$vars->data = $data;	
		H::vars($vars);
		
		H::js(array(
			'jquery/jquery.ui.widget.js',
			'jquery/jquery.ui.core.js',
			'jquery/jquery.ui.datepicker.js',
			'jquery/jquery.ui.datepicker-pt-BR.js',
			'jquery.timepicker.min.js',
			'deposito.js'
			));
		H::css(array('smoothness/jquery-ui.css','jquery.timepicker.css'));
		H::config('deposito.php','Endere�os');
		self::render();
	}
	
	public static function add_contato(){  H::render('paginas/contato/add.php'); }
	public static function form_contato(){  H::render(H::path().'form_contato.php'); }
	public static function form_endereco(){  H::render(H::path().'form_endereco.php'); }
	
	public static function rm_contato(){ 
		$model = new ClienteContato();
		$model->ID = URL::friend(3);
		if(!$model->remove()) die('N�o foi possivel excluir');
		H::redirect('usuario','edit',CLogin::id());
	}
	
	public static function rm_endereco(){ 
		$model = new ClienteEndereco();
		$model->ID = URL::friend(3);
		if(!$model->remove()) die('N�o foi possivel excluir');
		H::redirect('usuario','edit',URL::friend(2));
	}
}