<?php
class controller {

	public static function INIT() { 
		# PRIMEIRA DEFINI��O DE ID DE SESS�O
		if(!isset($_SESSION['idsession'])) CLogin::defSession();
		list($acao,$cod) = array(URL::friend(0),URL::friend(1));
		$cod = current(explode('.html',$cod));
		$acao = current(explode('.html',$acao));
		
		$SVARS = array('voltar_para_url','voltar_para_lista');
		foreach($SVARS as $SVAR)
			if(!isset($_SESSION[$SVAR])) 
				$_SESSION[$SVAR] = H::link('index.html');
		
		if($acao == 'sitemap-gen'){ self::sitemap_gen(); return true; }
		else { header('Content-type: text/html; charset=ISO-8859-1'); }
		
		if(!empty($acao)) H::acao(str_replace('-','_',$acao)); 
		else H::acao('index');

		if(!empty($cod)) H::cod($cod); 

		$vars = new stdClass();
		$vars->PCList = ProdutoCategoria::shortFindAll();
		$CTop = CarrinhoSession::basicInfoCarrinho(CLogin::IDSession());
		define('CARRINHO_VOLUMES',(int)$CTop->Volumes);
		define('CARRINHO_TOTAL',number_format($CTop->Total,2,',',''));
		H::vars($vars);
		H::title('',utf8_decode(SITE_TITLE),'-');
		
		# PASTA DOS ARQUIVOS DO MODULO
		H::path('paginas/');
		
		# JS GERAL DO MODULO
		H::js(array(
			'jquery/jquery.js',
			'jquery/jquery.fancybox.js',
			'jquery/jquery.fancybox-buttons.js',
			'global/H.js',
			'global/GridView.js',
			'index.js'
			));
		
		# CSS GERAL DO MODULO
		H::css(array(
			'jquery.fancybox.css',
			'jquery.fancybox-buttons.css',
			'grid.css',
			'index.css',
			'produtos.css',
			'endereco.css',
			'carrinho_de_compras.css',
			'login.css'
		));
				
		CLogin::getLogin();
		
		switch($acao){
			case 'carrinho':	return ctrlCarrinho::INIT();
			case 'fale-conosco':	return ctrlFaleConosco::INIT();
			case 'contato':	return ctrlContato::INIT();
			case 'usuario':		return ctrlUsuario::INIT();
			case 'endereco':	return ctrlEndereco::INIT();
			case 'login':	return ctrlLogin::INIT();
		}
		
		if(method_exists(get_class(),H::acao())): 
		
			$actions = array('minha_area','finalizar');
			$permissao = in_array(H::acao(),$actions);
			if(CLogin::id() == null && $permissao) return false;
			else call_user_func(get_class()."::".H::acao());
		else: return false; endif;
		
		return true;
	}
	
	
	private static function render() { 
		if(!isset($_GET['modal'])):
			H::render('paginas/layout/index.php'); 
		else:
			header('Content-type: text/html; charset=ISO-8859-1');
			H::render(H::path().H::file());
		endif;
	}
	
	#REDIRECIONA PARA UMA �REA PERMITIDA PELO USUARIO
	public static function index() { 
		URL::friend(0,'galeria');		
		self::galeria();
	}
	
	public static function galeria() { ctrlGaleria::INIT();ctrlGaleria::lista();}
	
	
	public static function h_js() { 
		H::path('paginas/h_js/');
		H::acao(URL::friend(1));
		H::cod(URL::friend(2));
		if(method_exists("ctrlH",H::acao())) call_user_func("ctrlH::".H::acao());
	}
	
	public static function logout() {  ctrlLogin::logout(); }
	
	public static function sitemap_gen() {  H::render('paginas/sitemap-gen.php'); }
}


