<?php
class ctrlFaleConosco {

	public static function INIT() {
		H::path('paginas/fale-conosco/');
		$acao = URL::friend(1);
		if(empty($acao)) $acao = 'index';
		$acao = str_replace('-','_',$acao);
		H::acao($acao);
		H::cod(URL::friend(2));
		H::css(array('fale-conosco.css'));
		
		if(method_exists(get_class(),H::acao())) call_user_func(get_class()."::".H::acao());
		else return false;
		
		return true;
	}
	
	private static function render() { 
		if(!isset($_GET['modal'])):
			H::render('paginas/layout/index.php'); 
		else:
			header('Content-type: text/html; charset=ISO-8859-1');
			H::render(H::path().H::file());
		endif;
	}
		
	public static function index() {
		$vars = new stdClass();
		$model = new FaleConosco();
		$vars->errors = array();
		if(count($_POST) > 1):
			$model->request('FaleConosco');
			$vars->errors = $model->Enviar();
			if(!count($vars->errors)):
				H::file('sucesso.php');
				self::render();
				return true;
			endif;
		endif;
		$vars->model = $model;
		H::vars($vars);
		H::file('form-contato.php');
		self::render();
		
				
	}	
	
	# CAPTCHA
	public static function gera_captcha(){	
		$captcha = new SimpleCaptcha();
		$captcha->CreateImage();
	}
	
}
