<?php
class ctrlContato {

	public static function INIT() {
		H::path('paginas/contato/');
		H::acao(URL::friend(1));
		H::cod(URL::friend(2));
		
		if(method_exists(get_class(),H::acao())) call_user_func(get_class()."::".H::acao());
		else return false;
		
		return true;
	}
	
	private static function render() { 
		if(!isset($_GET['modal'])):
			H::render('paginas/layout/index.php'); 
		else:
			header('Content-type: text/html; charset=ISO-8859-1');
			H::render(H::path().H::file());
		endif;
	}
		
	public static function create() { self::form(); }
	public static function edit() { 
		if(H::cod() == null) die('Sem ID');
		else self::form();
	}
	private static function form() { 
		
		if(isset($_POST['ClienteContato'])):
			ClienteContato::savePost('ClienteContato',CLogin::id());
			H::redirect($_GET['act']);
		endif;
		
		H::config('form.php',(H::cod() == null ? 'Criar Endere�o' : 'Editar Endere�o'));
		self::render();
	}
		
	public static function selecionar() { 
		H::config('list.php','Endere�os');
		self::render();
	}
	
	public static function remove() { 
		
		$model = new ClienteEndereco();
		$model->ID = H::cod();
		$model->IDCliente = CLogin::id();
		if(!$model->remove()) die('N�o foi possivel excluir');
		H::redirect($_GET['act']);
	}
}