<?php
class ctrlCarrinho {

	public static function INIT() {
		H::path('paginas/carrinho/');
		H::acao(URL::friend(1));
		H::cod(URL::friend(2));
		H::css(array('carrinho.css'));
		
		if(method_exists(get_class(),H::acao())) call_user_func(get_class()."::".H::acao());
		else return false;
		
		return true;
	}
	
	private static function render() { 
		if(!isset($_GET['modal'])):
			H::render('paginas/layout/index.php'); 
		else:
			header('Content-type: text/html; charset=ISO-8859-1');
			H::render(H::path().H::file());
		endif;
	}
	
	
	public static function cart() {
		$vars = new stdClass();
		$vars->data = CarrinhoSession::basicInfoCarrinho(CLogin::IDSession());
		$model = new CarrinhoItem();
		$model->IDSCarrinho = CLogin::IDSession();
		$vars->itens = $model->findAll();

		H::vars($vars);
		H::config('cart.php');
		self::render();
	}
	
	public static function quantidade() { 
		$model = new Produto();
		$model->IDProduto = H::cod();
		$produto = $model->findOne();
		H::vars(array('produto'=>$produto,'action'=>H::link('carrinho','add').'/'.H::cod().'?modal=true'));
		H::config('quantidade.php');
		self::render();
	}
	
	
	public static function remove() { 
		if(Carrinho::remover($_GET['src'])) H::redirect('carrinho','cart?modal=true'); 
		else die('erro');
	}
	
	public static function add() { 
		if(Carrinho::adicionar($_GET['src'])) H::redirect('carrinho','cart?modal=true'); 
		else die('erro');
	}
	
	public static function update_information() { 
		
		$CTop = CarrinhoSession::basicInfoCarrinho(CLogin::IDSession());
		
		$data = array();
		$data['VALOR_REF_VOLUMES']= (int)$CTop->Volumes;
		$data['VALOR_REF_TOTAL']= number_format($CTop->Total,2,',','');
		header('Content-type: application/json');
		echo json_encode($data);
	}
	

	public static function finalizar() { 
		$vars = new stdClass();
		$model = new ClienteEnderecoR();
		$model->IDCliente = CLogin::id();
		$model->IDEndereco = URL::friend(2);
		$vars->endereco = $model->findOne();
		$model = new Cliente();
		$model->IDCliente = CLogin::id();
		$vars->cliente = $model->findOne();
		$model = new CarrinhoItem();
		$model->IDSCarrinho = CLogin::IDSession();
		$vars->itens = $model->findAll(); 
		
		
		#$_SESSION['frete_valores'] = self::valoresFrete($vars->itens,$vars->endereco->CEP);
		#$vars->frete_valores = $_SESSION['frete_valores'];
		self::fecharCompra();
		
		H::vars($vars);
		
		H::config('finalizar.php','Finalizar Compra');
		self::render();
	}
		
	private static function fecharCompra() {
		$model = new Carrinho();
		$errors =  array();
		if(isset($_POST['Carrinho'])):
			$model->request('Carrinho');
			#$model->URLCobranca  = self::botaoDePagamento($model->FormaPagamento);
			$model->IDSCarrinho = $_SESSION['idsession'];
			#$model->ValorFrete = $_SESSION['frete_valores'][$model->CodFrete][2];
			$model->DataCadastro = date('Y-m-d H:i:s');
			$pag = new BotaoPagamento($model);
			$model->MP = $pag->MercadoPago(MP_ID,MP_KEY);
			$model->PS = $pag->PagSeguro(PS_ID,PS_KEY);
			$errors = Validate::model($model)->errors;
			if(count($errors) > 0):
				$temp = CarrinhoSession::basicInfoCarrinho(CLogin::IDSession());
				$model->Total = $temp->Total;
				$model->Volumes = $temp->Volumes;
				if(!($data = $model->save())): die('Erro ao salvar');
				else:
					# SEM ERROS E SALVO COM SUCESSO
					CLogin::defSession();
					H::redirect('usuario','compra',$data->IDCarrinho);
				endif;
			endif;
		endif;
		H::vars(array('model'=>$model,'errors'=>$errors));
	}
	
	private static function valoresFrete($itens,$CEP){
		$frete_valores = array();
		$frete_valores['41106'] = array('41106','PAC');
		$frete_valores['40010'] = array('40010','SEDEX');
		#$frete_valores['40045'] = array('40045','SEDEX a Cobrar');
		
		foreach($frete_valores as $k=>$opt):
			$frete = 0;
			foreach($itens as $i):
				$P = $i->produto();
				$teste = correio::frete($frete_valores[$k][0],'81630060',$CEP,$P->Peso,$P->Altura*100,$P->Largura*100,$P->Comprimento*100)*$i->Unidades;
				$frete += $teste;
			endforeach;
			$frete_valores[$k][2] = $frete;
			$frete_valores[$k][1] .= ' | R$ '.number_format($frete,2,',','');
		endforeach;
		return $frete_valores;
	}
	

}