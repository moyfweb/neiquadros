<?php
class ctrlH {

	private static function render() { 
		header('Content-type: text/html; charset=ISO-8859-1');
		H::render(H::path().H::file());
	}
	
	public static function confirm() { 
		H::config('confirm.php');
		self::render();
	}
}