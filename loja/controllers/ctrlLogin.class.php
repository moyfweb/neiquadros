<?php
class ctrlLogin {

	public static function INIT() {
		H::path('paginas/login/');
		$acao = URL::friend(1);
		if(empty($acao)) $acao = 'index';
		$acao = str_replace('-','_',$acao);
		H::acao($acao);
		H::cod(URL::friend(2));
		if(method_exists(get_class(),H::acao())):
			if(!in_array($acao,array('login','index','recuperar_senha','nova_senha')) && CLogin::id() < 1): 
				H::redirect('login','index');
			else:
				call_user_func(get_class()."::".H::acao());
			endif;
			
			return true;
		else: return false;
		endif;
	}

	private static function render() {
		if(!isset($_GET['modal'])):
			H::render('paginas/layout/index.php'); 
		else:
			header('Content-type: text/html; charset=ISO-8859-1');
			H::render(H::path().H::file());
		endif;
	}
		
	public static function index() { self::login(); }
	public static function login() { self::mkLogin('index'); }
	
	public static function recuperar_senha() {  
		$vars = new stdClass();
		$vars->errors = array();
		$model = new Cliente();
		$enviado = false;
		if(isset($_POST['Recuperar']['Email'])):
			$email = mysql_real_escape_string($_POST['Recuperar']['Email']);
			$model->addWhere("Email='$email'");
			$model = $model->findOne();
			if($model->IDCliente > 0):
				$model->Recuperacao = uniqid().md5(date('YmdHis'));
				$model->save();
				
				$cmail = sprintf('
				<h2>Ol� %s!</h2>
				<p>Voc� efetuou uma solicita��o de altera��o de senha!</p>
				<p><a href="%s" target="_blank" >Clique aqui para digitar uma nova senha!</a></p>',
				$model->Nome,
				URL::site().'login/nova-senha/?recuperacao='.$model->Recuperacao);
				$enviado = true;
				FormMail::send($email,'www.chibi.com.br - Recupera��o de senha.',$cmail,'nao-responda@chibi.com.br','Chibi Brinquedos',true);
			else:	
				$vars->errors['Email'] = 'Usu�rio n�o encontrado';
			endif;
		endif;
		if(!$enviado) H::config('recuperar-senha.php','Login');
		else H::config('verifique-email.php','Login');
		
		$vars->model = $model;
		H::vars($vars);
		self::render();
	}
	
	
	public static function nova_senha() {  
		$vars = new stdClass();
		$vars->errors = array();
		$model = new Cliente();
		if(CLogin::id() > 0):
			$model->IDCliente = CLogin::id();
		elseif(isset($_GET['recuperacao'])):
			$model->Recuperacao = $_GET['recuperacao'];
		else:
			H::config('solicitacao-nao-encontrada.php','Login');
			self::render();
			return 0;
		endif;
		$model = $model->findOne();
		
		if(empty($model->IDCliente)):
			H::config('solicitacao-nao-encontrada.php','Login');
			self::render();
			return 0;
		endif;
		
		if(isset($_POST['Recuperar']['Senha'])):
		
			if(strlen($_POST['Recuperar']['Senha']) < 6):
				$vars->errors['Senha'] = 'A senha deve conter 6 ou mais caracteres!';
			elseif($_POST['Recuperar']['Senha'] != $_POST['Recuperar']['SenhaB']):
				$vars->errors['SenhaB'] = 'As senhas n�o conferem!';
			else:
				$model->Senha = md5($_POST['Recuperar']['Senha']);
				if(!($data = $model->save())): die('Erro ao salvar');
				else: H::redirect('login','index');
				endif;
			endif;
			
		endif;
		$vars->model = $model;
		H::config('nova-senha.php','Login');
		H::vars($vars);
		self::render();
		return 1;
	}
	
	public static function senha_alterada() {  
		H::config('nova-senha.php','Login');
	}
	
	#public static function denied($msg=false) { H::config('denied.php','Acesso Negado'); self::render(); }
	public static function logout() { 
		session_destroy(); H::redirect('login','login'); 
	}
	
	public static function mkLogin($action) {
		$_SESSION['voltar_para_url'] = URL::atual();
		H::path('paginas/');
		$vars = new stdClass();
		if(URL::friend(0) != 'login') $vars->extra = 'autocomplete="off"';
		$vars->errors = array();
		$vars->data = new Cliente();
		$vars->action = URL::atual();
		$vars->hideLogin = true;
		$vars->form_id = 'form_login';
		
		
		if(isset($_POST['Login'])):
			$vars->errors = CLogin::submit( $_POST['Login'] );
			$vars->data->Email = $_POST['Login']['Email'];
			if(CLogin::id() > 0): 
				#H::redirect($action);
				#Log::record(CLogin::cliente(),"O usu�rio ".CLogin::nome()." se conectou.");
				header('Location: '.URL::atual());
			endif;
		endif;
		H::vars($vars);
		H::path('paginas/login/');
		H::config('login.php','Login');
		self::render();
	}
	
	public static function formLogin() {	
		$errors = array();
		$data = new Cliente();
		$action = H::link('usuario','index');
		$form_id = 'form_login2';
		include('paginas/login/login-form.php');
	}
}