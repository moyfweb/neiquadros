<?php
#Login::
class FaleConosco extends CModel {

	public $ID = null;
	public $Nome = null;
	public $Email = null;
	public $Assunto = null;
	public $Mensagem = null;
	public $Captcha = null;
	

	public function __construct() {
		/*
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('ID');
		$this->setTable('ecom_fale_conosco');
		#*/
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['ID'] = 'ID';
		$labels['Nome'] = 'Nome';
		$labels['Email'] = 'E-mail';
		$labels['Assunto'] = 'Assunto';
		$labels['Mensagem'] = 'Mensagem';
		$labels['Captcha'] = 'Captcha';
		return $labels[$key];
	}
	
	public function getType($key) {
		$types = array();
		$types['Nome'] = 'string';
		$types['Email'] = 'email';
		$types['Assunto'] = 'string';
		$types['Mensagem'] = 'string';
		if(isset($types[$key])) return $types[$key];
		else return false;
	}

	public function Enviar() {
		$errors = Validate::model($this)->errors;
		
		if($this->Captcha != $_SESSION['captcha']):
			$errors['Captcha'] = 'Captcha inv�lido!';
		endif;
		
		if(count($errors) > 0) return $errors;
		
		
		$cmail =  sprintf('
	%s: %s
	%s: %s
	%s: %s
	%s:
	%s
			',	$this->getLabel('Nome'),$this->Nome,
				$this->getLabel('Email'),$this->Email,
				$this->getLabel('Assunto'),$this->Assunto,
				$this->getLabel('Mensagem'),$this->Mensagem
				);
		$email = 'vendas@brtoys.com.br';
		$from = 'naoresponda@brtoys.com.br';
		FormMail::send($email,'Contato',$cmail,$from,'BR Toys',false);
		return $errors;
	}
	
}