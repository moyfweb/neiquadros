<?php
#Login::
class CLogin {

	private static $IDCliente = null;
	private static $Nome = null;
	private static $Email = null;
	
	public static function submit($post) {
		$errors = array();
		$model = new Cliente();
		$model->Email = $post['Email'];
		if($model->total() < 1 || empty($model->Email)) $errors['Email'] = 'Usuario n�o encontrado!';
		$model->Senha = md5(strtolower($post['Senha']));
		$model->addWhere('Status IN (1,2)');
		$data = $model->findOne();
		if(!empty($data->IDCliente)):
			#self::$HoraAcesso = date('Y-m-d H:i:s');
			$login = new stdClass();
			$keys = array('IDCliente','Nome','Email');
			foreach($keys as $k) $login->{$k} = $data->{$k};
			$_SESSION['LOGIN'] = $login;
			
			self::getLogin();
		elseif(!isset($errors['Login'])): $errors['Senha'] = 'Senha inv�lida!';
		endif;
		return $errors;
	}
	
	public static function getLogin() {
		if(isset($_SESSION['LOGIN'])):
			$keys = array('IDCliente','Nome','Email');
			foreach($keys as $k) self::$$k = $_SESSION['LOGIN']->{$k};
		endif;
	}
	/*
	public static function isNivel($N=0) {
		if(self::nivel() === null) return false;
		else if($N == 0) return true;
		else if(self::nivel() > $N) return false;
		else return true;
	}*/
	
	public static function id() { return self::$IDCliente; }
	public static function nome() { return self::$Nome; }
	public static function email() { return self::$Email; }
	public static function status() {
		$model = new Cliente();
		$model->IDCliente = self::$IDCliente;
		return $model->findOne()->Status;
	}
	
	public static function defSession() { $_SESSION['idsession'] = uniqid(); }
	
	public static function IDSession() { return $_SESSION['idsession']; }
}