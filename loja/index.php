<?php 
date_default_timezone_set("America/Sao_Paulo"); ## TIMEZONE
include('autoload.php');	## CARREGA CLASSES AUTOMATICAMENTE
$GLOBALS['AUTOLOAD'] = array(
	'../classes/models',
	'../classes/tools',
	'tools',
	'models',
	'controllers'
	);
session_name('ecmrc');
session_start();
if(!isset($_GET['location'])) $_GET['location'] = '';
define('LOCATION',$_GET['location']);
$access_contents = file_get_contents('../_db/access.json');
$access = (object)json_decode($access_contents, true);
foreach($access as $k=>$v) define($k,$v);

H::defaultConnection($access,$k,$v);
unset($access_contents,$access);
define('FILE_URL',URL::site());

H::root(URL::root());
H::site(URL::site());

if(!controller::INIT()):	
	echo "<H1>P�GINA N�O ENCONTRADA</H1>";
	#header("HTTP/1.0 404 Not Found");
	#header("Status: 404 Not Found");
	#header("Location: ".H::root().'404.html.php');
endif;
