$(document).ready(function() {
	$('#quantidade_produto').submit(function(){
		var estoque = $('#quantidade_produto .estoque span').html() * 1;
		var quantidade = $('#field_quantidade').val() * 1;
		
		if(quantidade > estoque) {
			alert('Existem apenas ' + estoque + ' produtos no estoque.');
			return false;
		} else {
			return H.popup.post(this);
		}
	});
});