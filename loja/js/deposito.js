$(document).ready(function() {
	var currentTime = new Date();
	var lastYear = currentTime.getFullYear() + 5;
	$( "input.datepicker" ).attr('readonly','readonly');
	$( "input.datepicker" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: currentTime.getFullYear() + ":" + lastYear
	});
	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
	$.datepicker.formatDate('dd-mm-yy');
	$('input.timepicker').timepicker({ 'step': 15,'timeFormat': 'H:i:s' });
	//$('input.timepicker').attr('readonly','readonly');
	$('input.datepicker').attr('readonly','readonly');
});