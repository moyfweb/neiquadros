$(document).ready(function(){
	/*
	$('#menu_principal li').hide();
	$('#menu_principal li.bt_show').show();
	$('#menu_principal li.bt_show').click(function(){
		if($('#menu_principal li:hidden').length > 0) {
			$('#menu_principal li').show();
			$('#menu_principal li.bt_show').show();
		} else {
			$('#menu_principal li').hide();
			$('#menu_principal li.bt_show').show();
		}
	});
		*/
	$('.fancybox-buttons').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		prevEffect : 'none',
		nextEffect : 'none',
		scrolling : 'no',
		closeBtn  : false,
		helpers : {
			title : {
				type : 'inside'
				},
		buttons	: {}
		},
		afterLoad : function() {
			this.title = 'Imagem ' + (this.index + 1) + ' de ' + this.group.length + (this.title ? ' - ' + this.title : '');
		}
	});
	$('#menu_produtos').hide();
	$('#menu_principal li a.produtos').click(function(){
		H.popup.open("<div id='menu_pop_up'>" + $('#menu_produtos').html() + "</div>");
		return false;
	});
	
	$('#menu_principal li a.carrinho').click(function(){
		H.popup.page(this);
		return false;
	});
	
	$('.p-box a.c_unidade, .p-box a.c_caixa, .p-detalhe a.c_unidade, .p-detalhe a.c_caixa').click(function(){
		H.popup.page(this);
		return false;
	});
	H.CallEveryone();
});

var Carrinho = {
	quantidade: function(){
		$('#form_quantidade').submit(function(){
			H.popup.post(this);
			return false;
		});
	},
	update: function(){
		var tmp_options = { url: ROOT + 'carrinho/update_information', dataType: 'json'};
		tmp_options.success = function(json) { 
			$('span.VALOR_REF_VOLUMES').html(json.VALOR_REF_VOLUMES);
			$('span.VALOR_REF_TOTAL').html(json.VALOR_REF_TOTAL);
		};
		$.ajax(tmp_options);
		Carrinho.checked_fields();
		return false;
	},
	remove: function(elm){
		if(confirm("Deseja excluir este item do carrinho?")) {  
			var tmp_target = 'div.fancybox-inner';
			var tmp_options = { url: $(elm).attr('href'), dataType: 'html', async: false };
			tmp_options.success = function(html) { $(tmp_target).html(html);  H.CallEveryone(tmp_target);};
			if(H.delay == 0) { H.delay = 1; $.ajax(tmp_options); }
		} 
		return false;
	},
	checked_fields: function(){
		$('#div_album .box a.buy').each(function(){
			var href=$(this).attr('src');
			if($('#carrinho_de_compras .box a.delbox[src=\'' + href + '\']').length > 0)
				$('#div_album .box a.buy[src=\'' + href + '\']').addClass('chkboxB');
			else
				$(this).removeClass('chkboxB');
		});
	}
}