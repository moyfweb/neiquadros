var Endereco = {
	INIT: function(){
		$('.CEP_Busca').click(function(){ return Endereco.busca_cep($(this).attr('field')); });
		$('#field_ClienteEndereco_CEP').mask('99999999');
		$('#form_endereco').submit(function(){
			var valid = true;
			if($('#field_ClienteEndereco_CEP').val().length != 8) {
				alert('O CEP deve conter 8 digitos.');
				return false;
			}
			
			var retorno = $('#form_endereco input.required').each(function(){
				if($(this).val() == '') { 
					console.log(42);
					var label = $(this).parent().find('label').html();
					alert(('O campo %s � obrigatorio.').replace('%s',label));
					valid = false;
					return false;
				}
			});
			return valid;
		});
			
	},
	busca_cep: function(field){
		var cep = $(field).val();
		var tmp_options = { url: ROOT + 'endereco/busca_cep/' + cep, dataType: 'json', async: false };
		tmp_options.success = function(json){Endereco.autocomplete(json)};
		$.ajax(tmp_options);
	},
	autocomplete: function(json) {
		if(json.uf.length < 1) {
			alert('Endere�o n�o encontrado');
		} else {
			$('#selecao_endereco .Estado').val(json.uf);
			$('#selecao_endereco .Cidade').val(json.cidade);
			$('#selecao_endereco .Bairro').val(json.bairro);
			$('#selecao_endereco .Logadouro').val(json.logadouro);
		}
	}
}