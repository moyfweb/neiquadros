<?php 
$form = new form();
$FD = array('ClienteEndereco');
	
$k = 'IDEndereco'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $FD[1] = $k;
$lines = array();
$lines[] = $form->hidden($FD,$l,$v);

$k = 'CEP';
$v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $FD[1] = $k; 
$DEF = array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'");
$lines[] = $form->text($FD,$l,$v,$DEF);

$lines[] = '<p><input type="button" value= "VERIFICAR CEP"  class="CEP_Busca" field="#field_ClienteEndereco_CEP"/></p>';
$lines[] = '<div class="clear"></div>';

$fields = array('Pais','Estado','Cidade','Bairro','Logadouro','Numero','Complemento');
foreach($fields as $k):
	$v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $FD[1] = $k; 
	if($k == 'Pais' && empty($v)) $v = 'Brasil';
	$req = array('Numero','Complemento');
	$class = !in_array($k,$req) ? $k.' required' : $k;
	$DEF = array('obrigatorio'=>1,'class'=>$class, 'type'=>"$t",'p'=>"class='$k'");
	$lines[] = $form->text($FD,$l,$v,$DEF);
endforeach;
	$k= 'Info';
	$v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $FD[1] = $k; 
	$DEF = array('obrigatorio'=>1,'class'=>$k, 'type'=>"$t",'p'=>"class='$k'");
	$lines[] = $form->textarea($FD,$l,$v,$DEF);

	
printf('
	<div id="selecao_endereco" class="autocall" fnc="Endereco.INIT()">
		<h1>%1$s</h1>
		<div class="linha formulario">
			%2$s
			%3$s
			<div style="clear: both"></div>
			<div class="actions"> 
				<a href="%4$s" class="voltar" > Voltar </a>
				<input type="submit" value="%5$s"/> 
				%7$s
			</div>
			%7$s
			%6$s
		</div>
	</div>
	',
	(!empty($model->IDEndereco) ?  'EDITAR ENDERE�O' : 'CRIAR ENDERE�O'),
	$form->openForm('endereco',URL::uri(),'form_endereco'," autocomplete='off'"),
	implode("\n\n",$lines),
	H::link($_GET['act']),
	(!empty($model->IDEndereco) ?  'Savar' : 'Criar'),
	$form->closeForm(),
	'<div style="clear: both"></div>'
	);
	
	