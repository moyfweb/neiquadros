
<?php 
$form = new form();
$FD = array('ClienteEndereco');
$model = new ClienteEnderecoR();
$model->setOrders('IDEndereco DESC');
$model->IDCliente = CLogin::id();
$enderecos = $model->findAll();
$html = '';
$rm_extra = " class='rm' onclick='return H.popup.confirm(this)' msg='Deseja mesmo remover este endereco?' ";

foreach($enderecos as $V):
	$usar = H::acao() == 'selecionar' ? tag::a(H::link('carrinho','finalizar',$V->IDEndereco),'Usar este',$V->CEP," class='usar'") : '';
	$fields = array('Pais','Estado','Cidade','Bairro','Logadouro','Numero','Complemento','CEP','Info');
	$inner = '';
	foreach($fields as $j):
	$inner .= sprintf("<p class='%s'><span>%s: </span> %s </p>\n\t\t\t\t",$j,$V->getLabel($j),$V->{$j});
	endforeach;
	$html .= sprintf("
		<div class='linha'>
			%1\$s
			<div style='clear: both'></div>
			<div class='actions'> 
				%2\$s
				%3\$s
				%4\$s
				<div style='clear: both'></div>
			</div>
			<div style='clear: both'></div>
		</div>
		",
		$inner,
		tag::a(H::link('endereco','remove',$V->ID,'?act='.URL::friend(0).'/'.URL::friend(1)),'Excluir','Excluir: '.$V->CEP,$rm_extra),
		tag::a(H::link('endereco','edit',$V->ID,'?modal=true&act='.URL::friend(0).'/'.URL::friend(1)),'Editar',$V->CEP," class='editar h_popup'  target='#selecao_endereco'"),
		$usar
	);
endforeach;
printf("
<div class='tab-title-white'>
	<h1>%s</h1>
</div>
<div class='endereco'>
<div id='selecao_endereco'>
	<div><a class='h_popup add_field_endereco' href='%s' target='#selecao_endereco'>Adicionar Endereco</a></div>
	%s
</div>
</div>
",
$tit,
H::link('endereco','create','?modal=true&act='.URL::friend(0).'/'.URL::friend(1)),
$html
);
?>