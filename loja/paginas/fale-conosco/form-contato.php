<div class='tab-title-gray'><h1>Contato</h1></div>
<div id="frm_fale_conosco">
	<div class="esquerda">
		<h2>Contato</h2>
		<p>Tel.: (41) 3042-4529</p>
		<p>Cel.: (41) 9664-1587</p>
		<p>Email.: vendas@brtoys.com.br</p>
		<p>Endereco: Rua Jo�o Soares Barcelos N� 73 </p>
		<p>Bairro: Vila Hauer</p>
		<p>Curitiba - PR</p>
		<iframe width="300" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?q=rua+henrique+mehl+51&ie=UTF8&ll=-25.483464,-49.222977&spn=0.00675,0.013078&hnear=Rua+Henrique+Mehl,+51+-+Uberaba,+Paran%C3%A1&t=m&z=17&output=embed&iwloc=near"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=pt-BR&amp;geocode=&amp;q=R.+Prof.+Jo%C3%A3o+Soares+Barcelos+N%C2%BA+73,+Curitib&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=51.04407,107.138672&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Prof.+Jo%C3%A3o+Soares+Barcelos,+73+-+Hauer,+Curitiba+-+Paran%C3%A1,+81630-060,+Rep%C3%BAblica+Federativa+do+Brasil&amp;t=m&amp;z=14&amp;ll=-25.474657,-49.257457" style="color:#0000FF;text-align:left">Exibir mapa ampliado</a></small>
	</div>
	<?php
	$form = new form();
	echo $form->openForm('FaleConosco',URL::uri(),'formulario_contato'," autocomplete='off' ");
	echo '<h2>Formul�rio</h2>';
	$field = array('FaleConosco');

	$k = 'Nome'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->text($field,$l,$v,array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));

	$k = 'Email'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->text($field,$l,$v,array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	
	$k = 'Assunto'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->text($field,$l,$v,array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	
	$k = 'Mensagem'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->textarea($field,$l,$v,array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	
	?>
	<div class="captcha">
		<div class="direita">
			<a href="#" onclick="
				document.getElementById('captcha').src=ROOT+'fale-conosco/gera-captcha?'+Math.random();
				document.getElementById('captcha-form').focus();return false;"
				id="change-image" style='display: block;'>Trocar texto!</a>
			<img src="<?php echo H::link('fale-conosco','gera-captcha');?>"  id='captcha'/>
		</div><!-- 
		CHANGE TEXT LINK 
		--><div  class="esquerda">
			<span> Digite o texto acima.</span>
			<input type="text" name="FaleConosco[Captcha]" id="captcha-form" class='valid_captcha'/>
		</div>
		<div style='clear: both;'></div>
		<span class="erro">
			<?php if(isset($errors['Captcha'])) echo $errors['Captcha'];?>
		</span>
	</div>
	<?php
		$submit = tag::a('#','<span>Enviar</span>','Enviar',"class='submit' onclick=\"$('#formulario_contato').submit();return false;\"");
		printf('<p>%s</p>',	$submit);
		echo $form->closeForm();
	?>
	<div style='clear: both;'></div>
</div>