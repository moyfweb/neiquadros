<?php
$form = new form();
$model = new UsuarioContatoR();
$model->ID = H::cod();
if(!empty($model->ID)) $model->findOne();
$field = array();
$tipos = ContatoTipo::getOptions();
$FD = array('ClienteContato');
$field[] = $form->openForm('contato',H::link('contato','create','?act='.$_GET['act']),'form_contato'," autocomplete='off'");
$k = 'IDContato'; $v = ''; $l = $model->getLabel($k); $t = $model->getType($k); $FD[1] = $k;
$field[] = $form->hidden($FD,$l,$v);

$k = 'IDTipo'; $v = ''; $l = $model->getLabel($k); $t = $model->getType($k); $FD[1] = $k;
$DEF = array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'");
$field[] = $form->select($FD,$l,$v,$tipos,$DEF);
	
$k = 'Valor'; $v = ''; $l = $model->getLabel($k); $t = $model->getType($k); $FD[1] = $k;
$DEF = array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'");
$field[] = $form->text($FD,$l,$v,$DEF);
$field[] = '<div><input type="submit" value="Salvar" /></div>';
$field[] = $form->closeForm();
printf('
	%s
	<div style="clear: both"></div>
	',
	implode("\n",$field)	);