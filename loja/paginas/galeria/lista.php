<div class='tab-title-gray'><h1>GALERIA</h1></div>
<div class='lista-prd' id="div_album">
<?php 
if(!isset($_GET['location'])) $_GET['location'] = '';
$parent_folders = explode('/',substr($_GET['location'],0,-1));
array_pop($parent_folders);
$up = implode('/',$parent_folders).'/';
if($up == '/') $up = '';
$count = 0;
if(!empty($_GET['location'])):
	printf('
	<div>
		<a href="%1$s" title="Voltar" class="parent">
			VOLTAR
		</a>
	</div>','?location='.$up,'Voltar');
endif;

foreach($files as $f):
	if($f->Tipo == 1):
	printf('
		<div class="box">
			<p><a href="%1$s" title="%2$s" class="dir">%2$s</a></p>
			<a href="%1$s" title="%2$s" class="thumb folder">
				&nbsp;
			</a>
		</div>
	',
		H::link('galeria.html?location='.$f->Folder.$f->File),
		$f->Name);
	else:
	$SRC = $f->Folder.$f->File;
	printf('
		<div class="box">
			<div class="actions">				
				<a href="%4$s" title="%2$s" class="zoom fancybox-buttons" data-fancybox-group="%6s">
				&nbsp;
				</a>
				<a href="%1$s" title="%2$s" src="%3$s" class="h_popup chkboxA buy %7$s">
				&nbsp;
				</a>
				<div class="clear"></div>
			</div>
			<a class="thumb h_popup" style="background-image: url(\'%5$s\')"  href="%1$s" >	&nbsp;</a>
			<p>%2$s</p>
			<div class="clear"></div>
		</div>
	',
	H::link('carrinho','add','?src='.urlencode($SRC)),
	$f->Name,
	$f->Folder.$f->File,
	$f->web(),
	$f->thumb(),
	'GP_'.$count,
	(Carrinho::verificar($SRC) ? 'chkboxB' : '')
	);
	endif;
	if($count%4==3):
		#echo '<div class="clear"></div>';
	endif;
	$count += 1;
endforeach;
echo '<div class="clear"></div>';
/*
printf('
<div class="pagination"><ul>%s %s %s %s</ul></div>',
	$model->linkPrev(),
	$model->getCurrentPages(),
	$model->getNavigationGroupLinks(),
	$model->linkNext()
	);
*/
?>
	<div class="clear"></div>
</div>
