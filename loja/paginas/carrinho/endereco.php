<?php 
global $FD, $id, $form;
$form = new form();
$FD = array('ClienteEndereco');
$model = new ClienteEnderecoR();

function cliente_endereco_mk_field($C) {
	global $FD, $id, $form;
	
	$k = 'IDEndereco'; $v = $C->{$k}; $l = $C->getLabel($k); $t = $C->getType($k); $FD[1] = $id; $FD[2] = $k; 
	$conteudo = "\n\n".$form->hidden($FD,$l,$v);
		
	$fields = array('Descricao','Pais','Estado','Cidade','Bairro','Logadouro','Numero','Complemento','CEP');
	foreach($fields as $k):
		$v = $C->{$k}; $l = $C->getLabel($k); $t = $C->getType($k); $FD[1] = $id; $FD[2] = $k; 
		$DEF = array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'");
		$conteudo .= "\n\n".$form->text($FD,$l,$v,$DEF);
	endforeach;
	return $conteudo;
}

if(URL::friend(2) == 'novo'):
	$id = uniqid();
	
	echo "
		<h1>NOVO ENDERE�O</h1>
		<p>Salve o novo endere�o para poder us�-lo como endere�o de envio.</p>
		<div class='linha'>
			".$form->openForm('Cliente',URL::uri(),'form_cliente'," autocomplete='off' ")."
			".cliente_endereco_mk_field($model)."
			
			<div class='actions'> 
				<input type='submit' value='Salvar'/> 
				<a href='".H::link('carrinho','endereco')."' class='voltar' > Voltar </a>
			<div style='clear: both'></div>
			</div>
			
			<div style='clear: both'></div>
			
			".$form->closeForm()."
		</div>
		";
elseif(is_numeric(URL::friend(2))):
	
	$model->setOrders('IDEndereco DESC');
	$V = $model->findOne();
	$id = uniqid();
		
	printf("
	<div class='linha'>
		%s
		%s
		<div class='actions'> 
			<input type='submit' value='Salvar'/> 
		<div style='clear: both'></div>
		</div>
		<div style='clear: both'></div>
		%s
	</div>
	",
	$form->openForm('Cliente',URL::uri(),'form_cliente'," autocomplete='off' "),
	cliente_endereco_mk_field($V),
	$form->closeForm()
	);
else:
	
	$model->setOrders('IDEndereco DESC');
	$enderecos = $model->findAll();
	$html = '';
	$rm_extra = " class='rm' onclick='return H.popup.confirm(this)' msg='Deseja mesmo remover este endereco?' ";
	foreach($enderecos as $V):
		$fields = array('Descricao','Pais','Estado','Cidade','Bairro','Logadouro','Numero','Complemento','CEP');
		$inner = '';
		foreach($fields as $j):
		$inner .= sprintf("<p class='%s'><span>%s: </span> %s </p>\n\t\t\t\t",$j,$V->getLabel($j),$V->{$j});
		endforeach;
		$html .= sprintf("
			<div class='linha'>
				%1\$s
				<div style='clear: both'></div>
				<div class='actions'> 
					%2\$s
					%3\$s
					%4\$s
					<div style='clear: both'></div>
				</div>
				<div style='clear: both'></div>
			</div>
			",
			$inner,
			tag::a(H::link('carrinho','finalizar',$V->IDEndereco),'Usar este',$V->Descricao," class='usar'"),
			tag::a(H::link('carrinho','endereco',$V->ID,'?modal=true'),'Editar',$V->Descricao," class='editar h_update'  target='#selecao_endereco'"),
			tag::a(H::link('carrinho','rm_endereco',$V->ID),'Excluir','Excluir: '.$V->Descricao,$rm_extra)
			);
	endforeach;
	printf("
	<div id='selecao_endereco'>
	<div><a class='h_update add_field_endereco' href='%s' target='#selecao_endereco'>Adicionar Endereco</a></div>
	<h1>SELECIONAR ENDERE�O</h1>
	%s
	</div>
	",
	H::link('carrinho','endereco','novo','?modal=true'),
	$html
	);
	
endif;
?>