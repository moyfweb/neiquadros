<div class='tab-title-principal'><h1>Fechamento da Compra</h1></div>
<div class='tab-title-white'>
	<h2>Informa��es da Compra</h2>
</div>

<div id='finalizar' class='div-conteudo'>

<table class='grid_view autocall ' fnc='GridView.zebra(this);'>
	<tr class="legenda">
		<!--th>ID</th-->
		<th>Nome</th>
		<th>Pre�o</th>
		<th>Unidades</th>
		<th>Soma</th>
	</tr>
	<?php 
	$soma = 0;
	foreach($itens as $i):
	printf('
	<tr class="grid">
		<!--td>%s</td-->
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>',
	$i->IDProduto,
	$i->produto()->Nome,
	number_format($i->Preco,2,',','') ,
	$i->Unidades,
	number_format($i->Preco*$i->Unidades,2,',','') 
	);
	$soma += $i->Preco*$i->Unidades;
	endforeach;
	printf('
		<tr class="soma">
			<!--td>&nbsp;</td-->
			<td colspan="3">&nbsp;</td>
			<td>Total: R$ %s</td>
		</tr>',number_format($soma,2,',',''));
	?>
</table>
</div>

<div class='tab-title-white'>
	<h2>Endere�o / Contato</h2>
</div>

<div id='endereco' class='div-conteudo'>
<table class='grid_view autocall' fnc='GridView.zebra(this);' >
<?php 
	$fields = array('Pais','Estado','Cidade','Bairro','Logadouro','Numero','Complemento','CEP','Info');
	foreach($fields as $k):
	printf('
	<tr class="grid">
		<td style="width:150px;font-weight: bold;">%s</td>
		<td>%s</td>
	</tr>',
	$endereco->getLabel($k),
	$endereco->{$k}
	);
	endforeach;
	printf('
	<tr class="grid">
		<td style="width:150px;font-weight: bold;">%s</td>
		<td>%s</td>
	</tr>',
	'E-mail de cobran�a',$cliente->Email);printf('
	<tr class="grid">
		<td style="width:150px;font-weight: bold;">%s</td>
		<td>%s</td>
	</tr>',
	'Telefone',$cliente->Telefone);
?>
</table>
</div>


<div class='tab-title-white'>
	<h2>Dados Adicionais</h2>
</div>

<div id='extra' class='div-conteudo'>
<?php

	$form = new form();
	echo "<input type='hidden' name='Carrinho[EnderecoEntrega]' value='<?php echo $endereco->IDEndereco;?>' />";
	echo $form->openForm('Carrinho',URL::uri(),'form_carrinho'," autocomplete='off' ");
	$field = array('Carrinho');
	
	$k = 'EnderecoEntrega'; $v = H::cod();$l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->hidden($field,$l,$v);
	
	#$options = CarrinhoFormaDePagamento::getOptions();
	#$k = 'FormaPagamento'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	#echo $form->select($field,$l,$v,$options,array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	
	
	$v = $model->{$k}; if(empty($v)) $v = $cliente->Email;
	$k = 'EmailCobranca'; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->hidden($field,$l,$v);
	#echo $form->text($field,$l,$v,array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	
	#Selecione o tipo de frete
	
	#$k = 'CodFrete'; $v = 41106; $l = 'Frete'; $t = 'integer'; $field[1] = $k;
	#echo $form->radio($field,$l,$v,$frete_valores,array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	
	$k = 'Observacoes'; $v = $model->{$k};$l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->textarea($field,$l,$v,array('obrigatorio'=>1,'class'=>'', 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	
	echo "<p style='text-align: right;'>
			<input type='submit' value='FECHAR COMPRA'/>
		</p>";
	$form->closeForm();

?>

</div>

