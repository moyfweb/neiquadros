<div id='carrinho_de_compras' style='min-height: 400px'>
<!-- CHAMA A FUN��O CARRINHO UPDATE -->
<span class='autocall' fnc='Carrinho.update()'></span>
<H1>Carrinho de Compras</H1>
<div class='lista-prd'>
<?php
$count=0; 
foreach($itens as $img):
	printf('
		<div class="box">
			<div class="actions">		
				<a href="%4$s" title="%2$s" class="zoom fancybox-buttons" data-fancybox-group="%6$s">
				&nbsp;
				</a>
				<a href="%1$s" src="%3$s" title="%2$s" class="h_popup delbox">
				&nbsp;
				</a>
				<div class="clear"></div>
			</div>
			<div class="thumb" style="background-image: url(\'%5$s\')" >
				&nbsp;
			</div>
			<p>%2$s</p>
			<div class="clear"></div>
		</div>
	',
	H::link('carrinho','remove','?src='.urlencode($img->SRC.$img->Foto)),
	$img->name(),
	$img->SRC.$img->Foto,
	$img->web(),
	$img->thumb(),
	'GP_'.$count
	);
	if($count%4==3):
		echo '<div class="clear"></div>';
	endif;
	$count += 1;
endforeach;

echo '<div class="clear"></div>';
?>
</div>
<?php 
if($data->Total > 49):
	echo tag::a(H::link('endereco','selecionar'),'FINALIZAR COMPRA',true," class='finalizar_compra' ");
else:
	echo '<h3 class="t-center" style="color:#900">Compra m�nima R$ 50,00</h3>';
endif;
?>
<?php 
echo tag::a($_SESSION['voltar_para_lista'],'VOLTAR A LISTA DE PRODUTOS',true," class='finalizar_compra' ");

?>


</div>