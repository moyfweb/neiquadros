<?php 
$base = '
	<url>
      <loc>http:%s</loc>
      <lastmod>%s</lastmod>
      <changefreq>%s</changefreq>
      <priority>%s</priority>
   </url>';
$model = new ProdutoCategoria();
$categorias = $model->findAll();
$content = '';
$site = substr(URL::site(), 0, -1);

$content .= sprintf($base,URL::site().'index.html',date('Y-m-d'),'daily','1.0'); 
$content .= sprintf($base,URL::site().'fale-conosco.html',date('Y-m-d'),'daily','1.0'); 
foreach($categorias as $c):
	$content .= 
	sprintf($base,
	$site.H::link('produtos',$c->URLAmigavel.'.html'),
	date('Y-m-d'),
	'monthly',
	'0.9'); 
endforeach;

$model = new Produto();
$produtos = $model->findAll();
foreach($produtos as $p):
	$content .= 
	sprintf($base,
	$site.H::link('produto',URL::friendly($p->Nome,$p->IDProduto)),
	substr($p->DataAtualizacao, 0, 10),
	'monthly',
	'0.8'); 
endforeach;
$xml = sprintf('<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
%s
</urlset>',$content);
$f = fopen('sitemap.xml','wb'); 
fwrite($f,$xml,strlen($xml)); 
fclose($f);