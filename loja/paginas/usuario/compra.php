<div class='tab-title-green'>
	<h1>Minha Compra em - 
	<?php echo CData::format('d/m/Y',$data->DataCadastro).' as '.CData::format('H:i:s',$data->DataCadastro).' h';?></h1>
</div>

<div id='informacoes'>

<h2>Detalhes da Compra</h2>
<table class='grid_view autocall' fnc='GridView.zebra(this);'>
	<tr class="legenda">
		<!--th>ID</th-->
		<th>Nome</th>
		<th>Pre�o</th>
		<th>Unidades</th>
		<th>Soma</th>
	</tr>
	<?php 
	foreach($itens as $i):
	printf('
	<tr class="grid">
		<!--td>%s</td-->
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
		<td>%s</td>
	</tr>',
	$i->IDProduto,
	$i->produto()->Nome,
	number_format($i->Preco,2,',','') ,
	$i->Unidades,
	number_format($i->Preco*$i->Unidades,2,',','') 
	);
	endforeach;
	printf('
		<tr class="soma">
			<td colspan="4">Total: R$ %s</td>
		</tr>',
		number_format($info->Total,2,',','')
		);
	?>
</table>

<?php 
	
	if($data->SituacaoVenda > 1):
		printf('<h2>%s</h2><h3>%s</h3>'.PHP_EOL,$data->getLabel('SituacaoVenda'),CarrinhoSVenda::getSituacao($data->SituacaoVenda));
	else:
	
		$pag_tgt = 'target="_blank"';
		$pag_img = tag::img('arquivos/layout/pagamentos/pagseguro.png','Pagar com PagSeguro');
		$pag_url = sprintf('https://pagseguro.uol.com.br/v2/checkout/payment.html?code=%s',$data->PS);
		$pgt = tag::a($pag_url,$pag_img,'Pagar com Pag Seguro',$pag_tgt);
		$pag_img = tag::img('arquivos/layout/pagamentos/mercadopago.png','Pagar com Mercado Pago');
		$pag_url = sprintf('https://www.mercadopago.com/mlb/checkout/pay?pref_id=%s',$data->MP);
		$pgt .= PHP_EOL .tag::a($pag_url,$pag_img,'Pagar com Mercado Pago',$pag_tgt);
		$pag_img = tag::img('arquivos/layout/pagamentos/deposito.png','Dep�sito Banc�rio');
		$pag_url = H::link('usuario','deposito',H::cod());
		#$pgt .= PHP_EOL .tag::a($pag_url,$pag_img,'Dep�sito Banc�rio');
		if(CLogin::status() == 1):
		printf('
		<h2>Page com:</h2>
			<div class="pagamento">
			%s
			<div class="clear"></div>
		</div>',$pgt);
		else:
		<?php echo ;?>
			printf('<p>Para efetuar o pagamento seu usu�rio precisa ser aprovado, %s</p>',tag::a(H::link('fale-conosco.html'),'clique aqui para entrar em contato.','Endere�o'));
		endif;
	endif;
	/*
	$mp = new MP(MP_ID,MP_KEY);
	$filters = array('external_reference'=>$data->IDSCarrinho);
	$searchResult = $mp->search_payment($filters);
	echo '<pre>';
	var_dump($searchResult);
	echo '</pre>';
	*/
	/* Definindo as credenciais  */    

	switch($data->SituacaoVenda) {
		case 1: 
			$payment_st = 'Estamos aguardando a confirma��o do pagamento para realizar a entrega de seu produto.';
			break;
		case 2:
			$payment_st = 'Pagamento sendo an�lizado pela prestadora de servi�o de pagamento (Pag Seguro ou Mercado Pago).';
			break;
		case 3:
			$payment_st = 'Pagamento realizado com sucesso em breve faremos a entrega, e enviaremos por e-mail o c�digo dos correios.';
			break;
		default:
			$payment_st = 'error';
	}
	printf('<div class="payment">%s</div>',$payment_st);
	
	if($data->SituacaoEntrega > 1):
		printf('<h2>%s</h2><h3>%s</h3>',$data->getLabel('SituacaoEntrega'),CarrinhoSEntrega::getSituacao($data->SituacaoEntrega)); 
	endif;
?>
</div>