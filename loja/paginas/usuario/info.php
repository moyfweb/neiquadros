<div class='tab-title-principal'><h1>Meus Dados</h1></div>
<div class='tab-title-white'>
	<h2>Informações</h2>
</div>

<div id='informacoes'>

<table class='grid_view autocall ' fnc='GridView.zebra(this);'>
<?php 
	$params = CObject::toArray($user);
	foreach($params as $K=>$V):
		if(in_array($K,array('IDCliente','Senha'))) continue;
		if(!is_array($V) && !is_object($V)):
		if($K == 'Status') $V = $V == 1 ? 'Ativo' :  'Inativo';
		echo "<tr class='grid'> 
			<td>".$user->getLabel($K)."</td>
			<td>".H::limit($V,200)."</td>
		</tr>";
		endif;
	endforeach; 
?>
</table>
</div>

<div class='tab-title-white'>
	<h2>Contato</h2>
</div>

<div id='contato'>
<?php 
	printf("<div><a class='h_popup add_field_contato' href='%s' target='#selecao_endereco'>Adicionar Contato</a></div>",
	H::link('contato','create','?modal=true&act='.URL::friend(0).'/'.URL::friend(1))
	);
	?>
<table class='grid_view autocall ' fnc='GridView.zebra(this);'>
<?php 
	foreach($contatos as $V):
		echo "<tr class='grid'> 
			<td>".$V->Tipo."</td>
			<td>".$V->Valor."</td>
		</tr>";
	endforeach;
?>
</table>

</div>

<?php 
	$model = new ClienteEnderecoR();
	$enderecos = $model->findAll();
	include('paginas/endereco/list.php');
?>