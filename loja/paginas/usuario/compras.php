<div class='tab-title-green'>
	<h1>Minhas Compras</h1>
</div>

<div id='informacoes'>
<table class='grid_view autocall' fnc='GridView.zebra(this);'>
<tr class="legenda"><th>Data</th><th>Hora</th><th>Valor</th><th>Situa��o</th><th>Action</th></tr>
<?php 
foreach($lista as $i):
printf('
<tr class="grid"><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>',
	CData::format('d/m/Y',$i->DataCadastro),
	CData::format('H:i:s',$i->DataCadastro),
	number_format($i->Total+$i->ValorFrete ,2,',','') ,
	CarrinhoSVenda::getSituacao($i->SituacaoVenda),
	tag::a(H::link('usuario','compra',$i->IDCarrinho),'Visualizar','Visualizar','class="view"')
);
endforeach;

?>
</table>


</div>