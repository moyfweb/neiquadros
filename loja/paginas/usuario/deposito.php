<div class='tab-title-green'>
	<h1>Minha Compra em - 
	<?php echo CData::format('d/m/Y',$data->DataCadastro).' as '.CData::format('H:i:s',$data->DataCadastro).' h';?></h1>
</div>

<div id='informacoes'>
	<h2>Conta</h2>
	<div class='conta-dep'>
	<?php printf('
	<p><strong>Banco:</strong> %s</p>
	<p><strong>Nome do Titular:</strong> %s</p>
	<p><strong>N� da conta:</strong> %s</p>
	<p><strong>Ag�ncia:</strong> %s</p>
	',
	DEP_BANCO, DEP_NOME, DEP_CONTA, DEP_AGENCIA );
	?>
	</div>
	<?php
if($salvo_deposito):
	echo '
	<div class="warn">
		<p>A mensagem foi enviada, ap�s a confirma��o lhe enviaremos um e-mail.</p>
	</div>';
elseif($sel_deposito):
	echo '
	<div class="warn">
		<p>Voc� acaba de receber um e-mail com os dados para fazer o dep�sito, ap�s efetua-lo clique no link "Confirma��o de dep�sito" que est� no E-mail.</p>
	</div>';
else:
	$form = new form();
	echo '<h2>Confirma��o de Dep�sito</h2>';
	echo $form->openForm('Deposito',URL::uri(),'form_deposito'," autocomplete='off' ");
	$field = array('Deposito');
				
	$k = 'Numero'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->text($field,$l,$v,array('obrigatorio'=>1,'class'=>$k, 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));

	$options = Deposito::optionsTipos();
	$k = 'Tipo'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->select($field,$l,$v,$options,array('obrigatorio'=>1,'class'=>$k, 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	echo '<div class="clear"></div>';
	
	$k = 'Data'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->text($field,$l,$v,array('obrigatorio'=>1,'class'=>"$k datepicker", 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	
	$k = 'Hora'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->text($field,$l,$v,array('obrigatorio'=>1,'class'=>"$k timepicker", 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	
	$k = 'Observacoes'; $v = $model->{$k}; $l = $model->getLabel($k); $t = $model->getType($k); $field[1] = $k; 
	echo $form->textarea($field,$l,$v,array('obrigatorio'=>1,'class'=>$k, 'type'=>"$t",'p'=>"class='$k'", 'errors'=>$errors));
	
	echo '<div class="clear"></div>';
	echo tag::a('#','<span>Enviar</span>','Enviar',"class='botao' onclick=\"$('#form_deposito').submit();return false;\"");
	echo $form->closeForm();
	

endif;
?>
		
	<h2>Detalhes da Compra</h2>
	<table class='grid_view autocall' fnc='GridView.zebra(this);'>
		<tr class="legenda">
			<!--th>ID</th-->
			<th>Nome</th>
			<th>Pre�o</th>
			<th>Unidades</th>
			<th>Soma</th>
		</tr>
		<?php 
		$soma = 0;
		foreach($itens as $i):
		printf('
		<tr class="grid">
			<!--td>%s</td-->
			<td>%s</td>
			<td>%s</td>
			<td>%s</td>
			<td>%s</td>
		</tr>',
		$i->IDProduto,
		$i->produto()->Nome,
		number_format($i->Preco,2,',','') ,
		$i->Unidades,
		number_format($i->Preco*$i->Unidades,2,',','') 
		);
		$soma += $i->Preco*$i->Unidades;
		endforeach;
		printf('
			<tr class="soma">
				<td colspan="4">Subtotal: R$ %s</td>
			</tr>
			<tr class="soma">
				<td colspan="4">Frete: R$ %s</td>
			</tr>
			<tr class="soma">
				<td colspan="4">Total: R$ %s</td>
			</tr>',
			number_format($soma,2,',',''),
			number_format($data->ValorFrete,2,',',''),
			number_format($soma+ (float)$data->ValorFrete,2,',','')
			);
		?>
	</table>
</div>
