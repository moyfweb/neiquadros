<?php
/*
	This class was created to reduce weight of __autoload.
*/
if(!defined('PAG_SEGURO')):
	$dirs = array('resources','config','service','utils','log','domain','exception','parser','helper');
	$count = 0;
	foreach($dirs as $D):
		# FILE PRIORITY FIX FOR PAG SEGURO
		switch($count){
			case 9:
				require_once __DIR__ .'/domain/PagSeguroCredentials.class.php';
				break;
			case 40:
				require_once __DIR__ .'/parser/PagSeguroServiceParser.class.php';
				require_once __DIR__ .'/parser/PagSeguroPaymentParser.class.php';
				break;
		}
		
		if ($handle = opendir(__DIR__ .'/'.$D.'/')):
			while (false !== ($file = readdir($handle))):
				if(substr_count($file,'.class.php') > 0):
					$class = __DIR__ .'/'.$D.'/'.$file;
					$count++;
					require_once $class;
				endif;
			endwhile;
		endif;
	endforeach;
	require_once('PagSeguroLibrary.php');
	define("PAG_SEGURO","true");
endif;
