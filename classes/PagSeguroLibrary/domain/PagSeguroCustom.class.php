<?php
class PagSeguroCustom {
	
    const SERVICE_NAME = 'transactionSearchService';
    public static function searchByReference(
        PagSeguroCredentials $credentials,
        $reference
    ) {

        LogPagSeguro::info("PagSeguroTransactionSearchService.searchByReference( reference=$reference )" );

        $connectionData = new PagSeguroConnectionData($credentials, self::SERVICE_NAME);
        $url = $connectionData->getServiceUrl();

        $searchParams = array('reference' => $reference);

		$buildURL = "{$url}/?" . $connectionData->getCredentialsUrlQuery()."&reference={$reference}";
        try {

            $connection = new PagSeguroHttpConnection();
            $connection->get(
                $buildURL,
                $connectionData->getServiceTimeout(),
                $connectionData->getCharset()
            );

            $httpStatus = new PagSeguroHttpStatus($connection->getStatus());

            switch ($httpStatus->getType()) {

                case 'OK':
                    $searchResult = PagSeguroTransactionParser::readSearchResult($connection->getResponse());
                    LogPagSeguro::info("PagSeguroTransactionSearchService.SearchByDate(reference=" .$reference);
                    break;

                case 'BAD_REQUEST':
                    $errors = PagSeguroTransactionParser::readErrors($connection->getResponse());
                    $e = new PagSeguroServiceException($httpStatus, $errors);
                    LogPagSeguro::error(
                        "PagSeguroTransactionSearchService.SearchByDate(initialDate=" .
                        PagSeguroHelper::formatDate($initialDate) .
                        ", finalDate=" . PagSeguroHelper::formatDate($finalDate) .
                        ") - end " . $e->getOneLineMessage()
                    );
                    throw $e;
                    break;

                default:
                    $e = new PagSeguroServiceException($httpStatus);
                    LogPagSeguro::error(
                        "PagSeguroTransactionSearchService.SearchByDate(initialDate=" .
                        PagSeguroHelper::formatDate($initialDate) . ", finalDate=" .
                        PagSeguroHelper::formatDate($finalDate) . ") - end " .
                        $e->getOneLineMessage()
                    );
                    throw $e;
                    break;

            }

            return isset($searchResult) ? $searchResult : false;

        } catch (PagSeguroServiceException $e) {
            throw $e;
        }
        catch (Exception $e) {
            LogPagSeguro::error("Exception: " . $e->getMessage());
            throw $e;
        }

    }
	
}