<?php
class Pagamento {
	private static function verificarMercadoPago($IDSCarrinho){
		$mp = new MP(MP_ID,MP_KEY);
		$filters = array('external_reference'=>$IDSCarrinho);
        $searchResult = $mp->search_payment($filters);
		
		$IDStatus = 0;
		$Status = array(0=>'0','pending'=>0,'in_process'=>1,'approved'=>2,'rejected'=>0,'cancelled'=>0);
		$res = $searchResult["response"]["results"];
		
		if(count($res) > 0): 
			$IDStatus = $res[0]["collection"]["status"];
		endif;
		return (int)$Status[$IDStatus];
	}
	
	private static function verificarPagSeguro($IDSCarrinho){
		include '../classes/PagSeguroLibrary/autoload.php';
		
		$credentials = new PagSeguroAccountCredentials(PS_ID,PS_KEY);
		$transaction = PagSeguroCustom::searchByReference($credentials,$IDSCarrinho);  
		$tr = $transaction->getTransactions();
		
		$IDStatus = 0;
		$Status = array(0,0,1,2,2,2,2,2);
		if(count($tr) > 0): 
		$IDStatus = (int)$tr[0]->getStatus()->getValue();
		endif;
		return $Status[$IDStatus];
	}
	
	public static function verificarPagamento($IDSCarrinho){
		$payment_status = array();
		$payment_status[0] = self::verificarPagSeguro($IDSCarrinho);
		$payment_status[1] = self::verificarMercadoPago($IDSCarrinho);
		sort($payment_status);
		$payment = end($payment_status);
		return $payment;
	}
}