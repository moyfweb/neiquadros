<?php
class BotaoPagamento {
	private $Carrinho;
	private $itens;
	
	public function __construct($Carrinho) {		
		$this->Carrinho = $Carrinho;
		$model = new CarrinhoItem();
		$model->IDSCarrinho = $Carrinho->IDSCarrinho;
		$this->itens = $model->findAll();
		
	}
	
	public function MercadoPago($MPID,$MPSecret){
		$mp = new MP($MPID,$MPSecret);
		$config = array();
		$total = 0;
		foreach($this->itens as $I):
			$total += (float)$I->Preco;
		endforeach;
		#$total += (float)$this->Carrinho->ValorFrete;
		$title = SITE_TITLE.' - REF: '.$this->Carrinho->IDSCarrinho;
		$config["items"] = array( 
				array(
				"title" => utf8_encode($title),
				"quantity" => 1,
				"currency_id" => "BRL",
				"unit_price" =>  $total
				)
			);
			
		$config["external_reference"] = $this->Carrinho->IDSCarrinho;
		$get_result = $mp->create_preference($config);
		parse_str(parse_url($get_result["response"]["init_point"], PHP_URL_QUERY), $variables);
		return $variables['pref_id'];
	}
	
	public function PagSeguro($PSEmail,$PSSecret){
		require_once '../classes/PagSeguroLibrary/autoload.php';
		$paymentRequest = new PagSeguroPaymentRequest(); 
		$paymentRequest->setCurrency("BRL");
		#$paymentRequest->addParameter('notificationURL', 'http://www.chibi.com.br/notificacao');
		$paymentRequest->setReference($this->Carrinho->IDSCarrinho);  
		foreach($this->itens as $I):
			$produto = $I->produto();
			$paymentRequest->addItem($produto->IDProduto,$produto->Nome,(int)$I->Unidades, (float)$I->Preco);
		endforeach;
		#$paymentRequest->addItem('0000','VALOR FRETE',1,(float)$this->Carrinho->ValorFrete);
		$credentials = new PagSeguroAccountCredentials($PSEmail,$PSSecret); 
		$url = $paymentRequest->register($credentials);
		$url_data = parse_url($url);
		parse_str(parse_url($url, PHP_URL_QUERY), $variables);
		return $variables['code'];
	}
}