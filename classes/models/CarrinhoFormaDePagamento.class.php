<?php
class CarrinhoFormaDePagamento extends CModel
{
	public $ID = null;
	public $Descricao = null;
	public $Status = null;
	
	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('ID');
		$this->setTable('ecom_carrinho_forma_pagamento');
		$this->addWhere('Status > -1');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['ID'] = 'PK';
		$labels['Descricao'] = 'Descri��o';
		$labels['Status'] = 'Status';
		return $labels[$key];
	}
	
	public function getType($key) {
		$types = array();
		$types['Descricao'] = 'string';		
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
	public static function getOptions(){
		$model = new self();
		$model->Status = 1;
		$lista = $model->findAll();
		foreach($lista as $v) $option[] = array($v->ID,$v->Descricao);
		return $option;
	}
	
	
}