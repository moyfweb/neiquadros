<?php
class Photos {
	
	private static $privado = '../fotos/privado/';
	private static $publico = '../fotos/publico/';
	private static $url = 'publico/';
	
	
	public static function findAll() {
		if(!isset($_GET['location'])) $_GET['location'] = '';
		$dir = self::$privado.$_GET['location'];
		$folders = array();
		$pictures = array();
		if ($handle = opendir(self::$privado.$_GET['location'])):
			while (false !== ($file = readdir($handle))):
				if (!in_array($file,array('.','..'))):
					if(is_dir($dir.$file)):
						$folder = new stdClass();
						$folder->location = $_GET['location'].$file.'/';
						$folder->name = $file;
						$file_url = self::$publico.$_GET['location'].$file;
						if(!file_exists($file_url)) mkdir($file_url,0777);
						$folders[] = $folder;
					else:
						$picture = new stdClass();
						$picture->location = $_GET['location'].$file;
						$picture->ext = end(explode('.',$file));
						$picture->ext = strtolower($picture->ext);
						$valid_ext = array('jpg','jpeg','png','gif','bmp');
						if(in_array($picture->ext,$valid_ext)):
							$picture->name = substr($file, 0, (-1*strlen($picture->ext)) - 1);
							$picture->file = $file;
							$picture->src = $_GET['location'].$file;
															
							$picture->thumb =self::duplicate($dir.$file,'_thumb',$picture->name,$picture->ext,200,150);
							$picture->web =self::duplicate($dir.$file,'_web',$picture->name,$picture->ext,600,450,'../fotos/carimbo_web.png');
							
							$pictures[] = $picture;
						endif;
					endif;
				endif;
			endwhile;
			closedir($handle);
		endif;
		return array($folders,$pictures);
	}
	
	
	public static function duplicate($temp,$folder,$name,$ext,$width,$height,$wm=null) {
		$file = "$name.$ext";
		$dir = self::$publico.$_GET['location'].$folder;
		if(!file_exists($dir)) mkdir($dir,0777);
		$pub = self::$publico.$_GET['location'].$folder.'/';
		if(!file_exists($pub.$file)) self::limitSize($temp,$pub,$name,$ext,$width,$height,$wm);
		$pub_url = H::root().self::$url.$_GET['location'].$folder.'/';
		return $pub_url.$file;
	}
	
	public static function limitSize($temp,$folder,$name,$ext,$width,$height,$wm=null) {
				
		$verot = new VerotImagePlugin($temp,'pt_BR');
		$verot->allowed = array('image/pjpeg','image/jpeg','image/jpg','image/x-png','image/png','image/gif');
		$verot->file_new_name_body = $name;
		$verot->file_new_name_ext = $ext;
		$verot->jpeg_quality = 50;
		$verot->image_resize = true;
		$verot->file_auto_rename = false;
		$verot->image_ratio_crop = false;
		$verot->image_default_color = '#000000';
		$verot->image_watermark = $wm;
		
		//var_dump($verot->error);die;
		if($width && $height):
			$verot->image_x	= $width;
			$verot->image_y	= $height;
			$verot->image_ratio_no_zoom_in = true;
		elseif($width):
			$verot->image_x	= $width;
			$verot->image_y	= 10 * $width;
			$verot->image_ratio_no_zoom_in = true;
		elseif($height):
			$verot->image_y	= $height;	
			$verot->image_x	= 10 * $height;	
			$verot->image_ratio_no_zoom_in = true;			
		endif;
		$verot->process($folder);
	}
	
	public static function publico(){ return self::$publico;}
	public static function privado(){ return self::$privado;}
	public static function url(){ return self::$url;}
}