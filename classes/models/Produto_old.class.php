<?php
class Produto extends CModel
{
	public $IDProduto = null;
	public $IDCategoria = null;
	public $IDMatrix = null;
	public $Nome = null;
	public $REF = null;
	public $Estoque = null;
	public $Validade = null;
	public $Preco = null;
	public $Peso = null;
	public $Altura = null;
	public $Largura = null;
	public $Comprimento = null;
	public $Resumo = null;
	public $Descricao = null;
	public $DataCadastro = null;
	public $DataAtualizacao = null;
	public $Especificacoes = null;
	public $Extra = null;
	public $Status = null;

	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('IDProduto');
		$this->setTable('ecom_produto');
		$this->addWhere('Status > -1');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['IDProduto'] = 'ID';
		$labels['IDCategoria'] = 'Categoria';
		$labels['IDMatrix'] = 'Produto Matriz';
		$labels['DataCadastro'] = 'Data de Cadastro';
		$labels['DataAtualizacao'] = 'Data de Atualização';
		$labels['Nome'] = 'Nome';
		$labels['REF'] = 'REF';
		$labels['Estoque'] = 'Estoque';
		$labels['Resumo'] = 'Resumo';
		$labels['Descricao'] = 'Descrição';
		$labels['Especificacoes'] = 'Especificações';
		$labels['Extra'] = 'Outras Informações';
		$labels['Validade'] = 'Data de Validade';
		$labels['Preco'] = 'Preço';
		$labels['Peso'] = 'Peso (Kg)';
		$labels['Altura'] = 'Altura (m)';
		$labels['Largura'] = 'Largura (m)';
		$labels['Comprimento'] = 'Comprimento (m)';
		$labels['Status'] = 'Status';
		return $labels[$key];
	}

	public function getType($key) {
		$types = array();
		#	$types['IDCategoria'] = 'integer';
		$types['Nome'] = 'string';
		$types['Preco'] = 'float';
		$types['Resumo'] = 'string';
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
	public function request($key) {
		if(isset($_REQUEST[$key])):
			$busca = $_REQUEST[$key];
			foreach($busca as $k=>$v):
				if(!empty($v) && $this->getType($k) == 'date'): $this->{$k} = CData::setDateBr($v);
				elseif(!empty($v)): $this->{$k} = $v;
				endif;
			endforeach;
		endif;	
	}
	
	public function SRC($width=null,$height=null,$crop=false){
		
		if(empty($this->IDProduto)) return null;
		$pf = new ProdutoFotoR();
		$pf->IDProduto = $this->IDProduto;
		$pf->setOrders('ID ASC');
		return $pf->findOne()->SRC($width,$height,$crop);
	}
	
	public static function getOne($ID) {
		$model = new self();
		$model->IDProduto = $ID;
		return $model->findOne();
	}
	
	public function getPreco() {
		return $this->Preco;
	}
	
	
	
	private static function getOneMatrix($ID) {
		$model = new self();
		$model->IDMatrix = 0;
		$model->IDProduto = $ID;
		return $model->findOne();
	}
	
	public function findOne() {
		$data = parent::findOne();
		if($data->IDMatrix > 0):
			$replace = self::getOneMatrix($data->IDMatrix);
			$fields = array('IDCategoria','Preco','Peso','Altura','Largura','Comprimento','Descricao','Especificacoes','Extra','REF','Validade');
			foreach($fields as $F):
				if(empty($data->{$F}) || $data->{$F} == 0.000) $data->{$F} = $replace->{$F};
			endforeach;
		endif;
		return $data;
	}
	
	public function findOneData() {
		return parent::findOne();
	}
}
