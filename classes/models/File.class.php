<?php
class File extends CModel {

	public $IDFile = null;
	public $IDParent = null;
	public $Folder = null;
	public $File = null;
	public $Name = null;
	public $Tipo = null;
	public $Status = null;
	
	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('IDFile');
		$this->setTable('ecom_file');
		$this->addWhere('Status > -1');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['IDFile'] = 'PK';
		$labels['IDParent'] = 'Parent';
		$labels['Folder'] = 'Pasta';
		$labels['File'] = 'Arquivo';
		$labels['Name'] = 'Nome';
		$labels['Tipo'] = 'Tipo';
		$labels['Status'] = 'Status';
		return $labels[$key];
	}
	
	public function getType($key) {
		$types = array();
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
	
	public static function parentID() {
		$model = new self();
		$model->addWhere(sprintf("CONCAT(Folder, File)='%s'",LOCATION));
		$loc = LOCATION;
		if(empty($loc)): return('0');
		else: 
			$ID = $model->findOne()->IDFile;
			if(empty($ID)): die('PASTA NÃO ENCONTRADA');
			else: return $ID;
			endif;
		endif;
		
	}
	
	public static function fastSave($IDParent,$Folder,$File=null,$Name=null,$Tipo=2) {
		$model = new self();
		$model->IDParent = $IDParent;
		$model->addWhere(sprintf("Folder='%s'",$Folder));
		$model->addWhere(sprintf("File='%s'",$File));
		$model = $model->FindOne();
		if(empty($model->IDFile)): 
			$model = new self();
			$model->IDParent = $IDParent;
			$model->Folder = $Folder;
			$model->File = $File;
			$model->Name = $Name;
			$model->Tipo = $Tipo;
			if(!$data = $model->save()) die('Não foi possivel executar File::fastSave()');
			else return $data;
		else:
			return $model;
		endif;
	}
	
	public static function map() {
		$ID = self::parentID();
		$dir = Photos::privado().LOCATION;
		if ($handle = opendir(Photos::privado().LOCATION)):
			while (false !== ($file = readdir($handle))):
				if (!in_array($file,array('.','..'))):
					if(is_dir($dir.$file)):
						$folder = new stdClass();
						self::fastSave($ID,LOCATION,$file.'/',$file,1);
						$file_url = Photos::publico().LOCATION.$file;
						if(!file_exists($file_url)): mkdir($file_url,0777); endif;
					else:
						$ext = end(explode('.',$file));
						$ext = strtolower($ext);
						$valid_ext = array('jpg','jpeg','png','gif','bmp');
						if(in_array($ext,$valid_ext)):
							$name = substr($file, 0, (-1*strlen($ext)) - 1);
							self::fastSave($ID,LOCATION,$file,$name);
															
							self::duplicate($dir.$file,'_thumb',$name,$ext,200,150);
							self::duplicate($dir.$file,'_web',$name,$ext,600,450,'../fotos/carimbo_web.png');
						endif;
					endif;
				endif;
			endwhile;
			closedir($handle);
		endif;
		return true;
	}
	
	public function web(){	return $this->img('_web'); }
	public function thumb(){ return $this->img('_thumb'); }
	public function img($dir){ return H::root().Photos::url().LOCATION.$dir.'/'.$this->File; }
	
	public static function duplicate($temp,$folder,$name,$ext,$width,$height,$wm=null) {
		$file = "$name.$ext";
		$dir = Photos::publico().$_GET['location'].$folder;
		if(!file_exists($dir)) mkdir($dir,0777);
		$pub = Photos::publico().$_GET['location'].$folder.'/';
		if(!file_exists($pub.$file)) self::limitSize($temp,$pub,$name,$ext,$width,$height,$wm);
		$pub_url = H::root().Photos::url().$_GET['location'].$folder.'/';
		return $pub_url.$file;
	}
	
	public static function limitSize($temp,$folder,$name,$ext,$width,$height,$wm=null) {
				
		$verot = new VerotImagePlugin($temp,'pt_BR');
		$verot->allowed = array('image/pjpeg','image/jpeg','image/jpg','image/x-png','image/png','image/gif');
		$verot->file_new_name_body = $name;
		$verot->file_new_name_ext = $ext;
		$verot->jpeg_quality = 50;
		$verot->image_resize = true;
		$verot->file_auto_rename = false;
		$verot->image_ratio_crop = false;
		$verot->image_default_color = '#000000';
		$verot->image_watermark = $wm;
		
		//var_dump($verot->error);die;
		if($width && $height):
			$verot->image_x	= $width;
			$verot->image_y	= $height;
			$verot->image_ratio_no_zoom_in = true;
		elseif($width):
			$verot->image_x	= $width;
			$verot->image_y	= 10 * $width;
			$verot->image_ratio_no_zoom_in = true;
		elseif($height):
			$verot->image_y	= $height;	
			$verot->image_x	= 10 * $height;	
			$verot->image_ratio_no_zoom_in = true;			
		endif;
		$verot->process($folder);
	}
	
	public static function publico(){ return Photos::publico();}
}