<?php
class ClienteEnderecoR extends CModel
{ 	
	public $ID = null;
	public $IDCliente = null;
	public $IDEndereco = null;
	public $Pais = null;
	public $Estado = null;
	public $Cidade = null;
	public $Bairro = null;
	public $Logadouro = null;
	public $Numero = null;
	public $Complemento = null;
	public $CEP = null;
	public $Info = null;
	public $Status = null;
	
	public function __construct() {
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('ID');
		$this->setTable('ecom_cliente_endereco');
		$this->addWhere('Status > -1');
	}
	
	
	public function getLabel($key) {
		$labels = array();
		$labels['ID'] = 'ID';
		$labels['IDCliente'] = 'ID do Cliente';
		$labels['IDEndereco'] = 'ID do Endere�o';
		$labels['Info'] = 'Outras Informa��es';
		$labels['Pais'] = 'Pa�s';
		$labels['Estado'] = 'Estado';
		$labels['Cidade'] = 'Cidade';
		$labels['Bairro'] = 'Bairro';
		$labels['Logadouro'] = 'Logadouro';
		$labels['Numero'] = 'N�mero';
		$labels['Complemento'] = 'Complemento';
		$labels['CEP'] = 'CEP';
		$labels['Status'] = 'Status';
		return $labels[$key];
	}
	
	public function getType($key) {
		$types = array();
		#$types['ID'] = 'hidden';
		$types['IDCliente'] = 'integer';
		$types['IDEndereco'] = 'integer';
		$types['IDTipo'] = 'integer';
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
	private function fFrom(){
		$this->setFrom("
		FROM (
			SELECT  
			r.ID,
			r.IDCliente,
			r.IDEndereco,
			c.Nome as Cliente,
			e.Pais,
			e.Estado,
			e.Cidade,
			e.Bairro,
			e.Logadouro,
			e.Numero,
			e.Complemento,
			e.CEP,
			e.Info,
			r.Status
			FROM ecom_cliente_endereco as r
			LEFT JOIN ecom_cliente AS c ON c.IDCliente=r.IDCliente
			LEFT JOIN ecom_endereco AS e ON e.IDEndereco=r.IDEndereco
			WHERE r.IDCliente=".$this->IDCliente."
			GROUP BY r.ID
		) as t");
	
	}
	
	public function findAll() { 
		$this->fFrom();
		if(!empty($this->IDCliente)) return parent::findAll(); 
		else return array(new self(null));	
	}
	
	public function findOne() { 
		$this->fFrom();
		if(!empty($this->IDCliente)) return parent::findOne(); 
		else return new self();	
	}
}