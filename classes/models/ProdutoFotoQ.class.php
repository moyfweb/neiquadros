<?php
class ProdutoFotoQ extends CModel
{
	public $IDProduto = null;
	public $IDCategoria = null;
	public $CodMitryus = null;
	public $REF = null;
	public $Nome = null;
	public $Descricao = null;
	public $Preco = null;
	public $Caixa = null;
	public $Estoque = null;
	public $DataCadastro = null;
	public $DataExpiracao = null;
	public $HasFoto = null;
	public $Status = null;

	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('IDProduto');
		$this->setTable('ecom_produto');
		$this->addWhere('Status > -1');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['IDProduto'] = 'ID';
		$labels['IDCategoria'] = 'Categoria';
		$labels['CodMitryus'] = 'Cod Mitryus';
		$labels['REF'] = 'REF';
		$labels['Nome'] = 'Nome';
		$labels['Descricao'] = 'Descri��o';
		$labels['Preco'] = 'Pre�o';
		$labels['Caixa'] = 'Itens por Caixa';
		$labels['Estoque'] = 'Estoque';
		$labels['DataCadastro'] = 'Data de Cadastro';
		$labels['DataExpiracao'] = 'Data de Expira��o';
		$labels['HasFoto'] = 'Tem Foto';
		$labels['Status'] = 'Status';
		return $labels[$key];
	}

	public function getType($key) {
		$types = array();
		#	$types['IDCategoria'] = 'integer';
		$types['Nome'] = 'string';
		$types['Preco'] = 'float';
		$types['Caixa'] = 'integer';
		$types['DataExpiracao'] = 'date';
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
	public function bQuery(){
		$this->setSelect('SELECT 
	t.*,
	IF(pf.NFotos IS NULL,0,1) as HasFoto
		');
		$this->setFrom('FROM ecom_produto as t');
		$this->addJoin('LEFT JOIN (SELECT IDFoto as NFotos, IDProduto FROM ecom_produto_foto GROUP BY IDProduto) AS pf ON pf.IDProduto=t.IDProduto');
		
		if($this->HasFoto === 'NOT NULL') $this->addWhere('pf.NFotos IS NOT NULL');
		if($this->HasFoto === 'NULL') $this->addWhere('pf.NFotos IS NULL');
		$this->HasFoto = null;
		
	}
	
	public static function getOne($PK) {
		$model = new self();
		$model->IDProduto = $PK;
		return $model->findOne();
	}
	
	public function save() {
		$this->DataExpiracao = CData::setDateBr($this->DataExpiracao);
		return parent::save();
	}
	public function request($key) {
		if(isset($_REQUEST[$key])):
			$busca = $_REQUEST[$key];
			foreach($busca as $k=>$v):
				if(!empty($v)): $this->{$k} = $v;endif;
			endforeach;
		endif;	
	}
	
	public function SRC($width=null,$height=null,$crop=false){
		
		if(empty($this->IDProduto)) return null;
		$pf = new ProdutoFotoR();
		$pf->IDProduto = $this->IDProduto;
		$pf->setOrders('ID ASC');
		return $pf->findOne()->SRC($width,$height,$crop);
	}
}