<?php
class Deposito extends CModel
{
	public $IDDeposito = null;
	public $IDCarrinho = null;
	public $IDCliente = null;
	public $Numero = null;
	public $Data = null;
	public $Hora = null;
	public $Tipo = null;
	public $Observacoes = null;
	public $Status = null;
	
	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->IDCliente = CLogin::id();
		$this->setPK('IDDeposito');
		$this->setTable('ecom_carrinho_deposito');
		$this->addWhere('Status > -1');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['IDDeposito'] = 'PK';
		$labels['IDCarrinho'] = 'ID Carrinho';
		$labels['IDCliente'] = 'ID Cliente';
		$labels['Numero'] = 'N�mero do Dep�sito';
		$labels['Data'] = 'Data do Dep�sito';
		$labels['Hora'] = 'Hora do Dep�sito';
		$labels['Tipo'] = 'Tipo de Deposito';
		$labels['Observacoes'] = 'Observa��es';
		$labels['Status'] = 'Status';
		return $labels[$key];
	}
	
	public function getType($key) {
		$types = array();
		$types['IDCarrinho'] = 'integer';
		$types['IDCliente'] = 'integer';
		$types['Numero'] = 'string';
		$types['Data'] = 'date';
		$types['Hora'] = 'string';
		$labels['Tipo'] = 'string';
		
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
		
	public static function optionsTipos(){
		$tipos = array('CHEQUE','DINHEIRO','TRANSFERENCIA','DOC','TEF');
		$options = array();
		$options[] = array('','Selecione um tipo de Deposito');
		foreach($tipos as $t) $options[] = array($t,$t);
		return $options;
	}

	public static function novaSolicitacao($IDCarrinho,$IDCliente){
		$model = new Carrinho();
		$model->IDCarrinho = $IDCarrinho;
		$data = $model->findOne();
		$model = new Cliente();
		$model->IDCliente = $IDCliente;
		$client = $model->findOne();
		$data->Deposito = 1;
		if(!$data->save()): 
			printf('Erro ao atualizar status "Deposito" em %s !',$IDCarrinho);
			die();
		endif;
		$pag_url = H::link('usuario','deposito',$IDCarrinho);
		
		$txt = 'Clique aqui para acessar a p�gina de confirma��o de dep�sito.';
		$cmail = sprintf('
		<h2>Ol� %s!</h2>
		<p>Voc� decidiu pagar sua compra no nosso site atrav�z de dep�sito banc�rio, segue abaixo os dados para deposito.</p>
		<h3>DADOS PARA O DEPOSITO</h3>
			<p><strong>Banco:</strong> %s</p>
			<p><strong>Nome do Titular:</strong> %s</p>
			<p><strong>N� da conta:</strong> %s</p>
			<p><strong>Ag�ncia:</strong> %s</p>
		<hr/>
		<p>Logo abaixo os link para a confirma��o de deposito.</p>
		<p>%s</p>',
		$client->Nome,
		tag::a($pag_url,$txt,'Dep�sito Banc�rio'),
		DEP_BANCO, DEP_NOME, DEP_CONTA, DEP_AGENCIA );
		
		$enviado = true;
		FormMail::send(
			$client->Email,
			'www.chibi.com.br - Dep�sito.',
			$cmail,
			'nao-responda@chibi.com.br',
			'Chibi Brinquedos',
			true
		);
	}

	public static function respostaConfirmacao($IDCliente){
		$model = new Cliente();
		$model->IDCliente = $IDCliente;
		$client = $model->findOne();
		$txt = 'Clique aqui para visualizar o pedido.';
		$cmail = sprintf('
		<h2>Ol� %s!</h2>
		<p>Seu comprovante de deposito foi enviado com sucesso, logo entraremos em contato!</p>
		<p>%s</p>',
		$client->Nome,
		tag::a($pag_url,$txt,'Dep�sito Banc�rio'));
		$enviado = true;
		FormMail::send(
			$client->Email,
			'www.chibi.com.br - Dep�sito.',
			$cmail,
			'nao-responda@chibi.com.br',
			'Chibi Brinquedos',
			true
		);
	}

	public static function Depositado($IDCarrinho){
		$model = new Deposito();
		$model->IDCarrinho = $IDCarrinho;
		if($model->total() > 0) return true;
		else return false;
	}
	
}