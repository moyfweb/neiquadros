<?php
class Endereco extends CModel
{
	public $IDEndereco = null;
	public $Info = null;
	public $Pais = null;
	public $Estado = null;
	public $Cidade = null;
	public $Bairro = null;
	public $Logadouro = null;
	public $Numero = null;
	public $Complemento = null;
	public $CEP = null;
	
	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('IDEndereco');
		$this->setTable('ecom_endereco');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['IDEndereco'] = 'ID';
		$labels['Info'] = 'Outras Informa��es';
		$labels['Pais'] = 'Pa�s';
		$labels['Estado'] = 'Estado';
		$labels['Cidade'] = 'Cidade';
		$labels['Bairro'] = 'Bairro';
		$labels['Logadouro'] = 'Logadouro';
		$labels['Numero'] = 'N�mero';
		$labels['Complemento'] = 'Complemento';
		$labels['CEP'] = 'CEP';
		return $labels[$key];
	}
	
	public function getType($key) { 
		$types = array();
		#	$types['IDCategoria'] = 'integer';
		$types['Pais'] = 'string';
		$types['Estado'] = 'string';
		$types['Cidade'] = 'string';
		$types['Bairro'] = 'string';
		$types['Logadouro'] = 'string';
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
	
	public static function requestSave($request) {
		$model = new self();
		$model->request($request);
		if(empty($model->IDEndereco)) $model->IDEndereco = null;
		
		$new = $model->IDEndereco == null ? true : false;
		if(!$data = $model->save()) die('N�o foi possivel executar Contato::save()');
		else return (!$new ? false : $data->IDEndereco);
		
	}
}