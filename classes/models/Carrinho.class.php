<?php
class Carrinho extends CModel
{
	public $IDCarrinho = null;
	public $IDSCarrinho = null;
	public $IDCliente = null;
	public $DataCadastro = null;
	public $SituacaoVenda = null;
	public $SituacaoEntrega = null;
	public $FormaPagamento = null;
	public $EnderecoEntrega = null;
	public $EmailCobranca = null;
	public $Total = null;
	public $MP = null;
	public $PS = null;
	public $Deposito = null;
	public $Observacoes = null;
	public $Status = null;
	
	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->IDCliente = CLogin::id();
		$this->setPK('IDCarrinho');
		$this->setTable('ecom_carrinho');
		$this->addWhere('Status > -1');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['IDCarrinho'] = 'PK';
		$labels['IDSCarrinho'] = 'Session';
		$labels['IDCliente'] = 'ID Cliente';
		$labels['Total'] = 'Total';
		$labels['DataCadastro'] = 'Data do Cadastro';
		$labels['SituacaoVenda'] = 'Situa��o da Venda';
		$labels['SituacaoEntrega'] = 'Situa��o da Entrega';
		$labels['FormaPagamento'] = 'Forma de Pagamento';
		$labels['EnderecoEntrega'] = 'Endere�o de Entrega';
		$labels['EmailCobranca'] = 'Email de Cobran�a';
		$labels['MP'] = 'URL Mercado Pago';
		$labels['PS'] = 'URL Pag Seguro';
		$labels['Deposito'] = 'Deposito';
		$labels['Observacoes'] = 'Observa��es';
		$labels['Status'] = 'Status';
		return $labels[$key];
	}
	
	public function getType($key) {
		$types = array();
		$types['IDSCarrinho'] = 'string';
		$types['IDCliente'] = 'integer';
		$types['DataCadastro'] = 'date';
		$types['FormaPagamento'] = 'integer';
		$types['EnderecoEntrega'] = 'integer';
		$types['EmailCobranca'] = 'email';
		$labels['MP'] = 'string';
		$labels['PS'] = 'string';
		
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
	public function adicionar($Produto,$Preco=10.00) {
		$carrinho = new CarrinhoItem();
		$break = explode('/',$Produto);
		$carrinho->Foto = array_pop($break);
		$carrinho->SRC = implode('/',$break).'/';
		$carrinho->Preco = $Preco;
		$carrinho->IDSCarrinho = CLogin::IDSession();
		$ID = self::verificar($Produto);
		if($ID): return true;
		else: return $carrinho->save();
		endif;
	}
	
	public function remover($Produto) {
		$ID = self::verificar($Produto);
		if($ID):
			$carrinho = new CarrinhoItem();
			$carrinho->ID = $ID;
			return $carrinho->delete();
		else: return true;
		endif;
	}
	
	public static function verificar($Produto) {
		$break = explode('/',$Produto);
		$Foto = array_pop($break);
		$SRC = implode('/',$break).'/';
		$carrinho = new CarrinhoItem();
		$carrinho->addWhere(sprintf("Foto = '%s'",$Foto));
		$carrinho->addWhere(sprintf("SRC = '%s'",$SRC));
		$carrinho->addWhere(sprintf("IDSCarrinho = '%s'",CLogin::IDSession()));
		$carrinho = $carrinho->findOne();
		if(!empty($carrinho->ID)): return $carrinho->ID;
		else: return false;
		endif;
	}
	
	public function findOne(){
		return parent::findOne();
	}
	
	public function findOneUpdate(){
		self::updatePayments($this->IDCarrinho);
		return parent::findOne();
	}
		
	public function findAllForPayments(){
		return parent::findAll();
	}
		
	public static function updatePayments($IDCarrinho=null){
		$model = new Carrinho();
		$model->IDCarrinho = $IDCarrinho;
		$model->addWhere('SituacaoVenda<3');
		$list = $model->findAllForPayments();
		#ALTERA O STATUS DE TODAS AS COMPRAS EFETUADAS
		foreach($list as $data):
			$pv = Pagamento::verificarPagamento($data->IDSCarrinho);
			if($data->SituacaoVenda == 1 and $pv == 1):
				$data->SituacaoVenda = 2;
				$data->save();
			elseif($pv == 2):
				$data->SituacaoVenda = 3;
				$data->save();
			endif;
		endforeach;
		$model->addWhere('(DataCadastro < DATE_SUB(NOW(),INTERVAL 30 DAY))');
		$list = $model->findAllForPayments();	
		#REMOVE COMPRAS ABANDONADAS A MAIS DE 45 DIAS.
		foreach($list as $data):
			$data->Status = -1;
			$data->save();
		endforeach;
	}

	
}