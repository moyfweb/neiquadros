<?php
class UF extends CModel
{
	public $ID = null;
	public $Codigo = null;
	public $Nome = null;
	public $UF = null;
	public $Regiao = null;
	
	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('UF');
		$this->setTable('ecom_uf');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['ID'] = 'ID';
		$labels['Codigo'] = 'C�digo';
		$labels['Nome'] = 'Nome';
		$labels['UF'] = 'UF';
		$labels['Regiao'] = 'Regi�o';
		return $labels[$key];
	}
	
	public function getType($key) {
		$types = array();		
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
	public static function getOptions(){
		$option = array(array('','UF'));
		$model = new self();
		$lista = $model->findAll();
		foreach($lista as $v) $option[] = array($v->UF,$v->UF);
		return $option;
	}
}