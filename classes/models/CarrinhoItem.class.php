<?php
class CarrinhoItem extends CModel
{
	public $ID = null;
	public $IDSCarrinho = null;
	public $SRC = null;
	public $Foto = null;
	public $Preco = null;
	public $Status = null;
	
	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('ID');
		$this->setTable('ecom_carrinho_item');
		$this->addWhere('Status > -1');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['ID'] = 'PK';
		$labels['IDSCarrinho'] = 'Session';
		$labels['SRC'] = 'Caminho do Arquivo';
		$labels['Foto'] = 'Nome do Arquivo';
		$labels['Preco'] = 'Pre�o';
		$labels['Status'] = 'Status';
		return $labels[$key];
	}
	
	public function getType($key) {
		$types = array();
		$types['SRC'] = 'string';
		$types['Foto'] = 'string';
		$types['Preco'] = 'float';
		
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
	public function name() {
		$brk = explode('.',$this->Foto);
		array_pop($brk);
		return implode('.',$brk);
	}
	
	public function web() {
		return H::root().Photos::url().$this->SRC.'_web/'.$this->Foto;
	}
	
	public function thumb() {
		return H::root().Photos::url().$this->SRC.'_thumb/'.$this->Foto;
	}
	
	/*public function save() {
		$carrinho = new self();
		$this->IDSCarrinho = $_SESSION['idsession'];
		$carrinho->IDSCarrinho = $this->IDSCarrinho;
		$carrinho->IDProduto = $this->IDProduto;
		$this->ID = $carrinho->findOne()->ID;
		return parent::save();
	}*/
			
	public function remove() {
		if(!empty($this->ID)):
			$this->Status = -1;
			if(!$data = parent::save()):	die('N�o foi poss�vel salvar!');
			else: return $data; endif;
		else:
			die('ID n�o encontrado');
		endif;
	}
	
}