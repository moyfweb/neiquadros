<?php
class CarrinhoSEntrega extends CModel
{
	public $ID = null;
	public $Situacao = null;
	public $Status = null;
	
	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('ID');
		$this->setTable('ecom_carrinho_sit_entrega');
		$this->addWhere('Status > -1');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['ID'] = 'PK';
		$labels['Situacao'] = 'Situa��o';
		$labels['Status'] = 'Status';
		return $labels[$key];
	}
	
	public function getType($key) {
		$types = array();
		$types['Situacao'] = 'string';		
		if(isset($types[$key])) return $types[$key];
		else return false;
	}
	
	public static function getOptions(){
		$option = array(array(0,'Selecione uma op��o'));
		$model = new self();
		$model->Status = 1;
		$lista = $model->findAll();
		foreach($lista as $v) $option[] = array($v->ID,$v->Situacao);
		return $option;
	}
	
	public static function getSituacao($ID){
		$model = new self();
		$model->ID = $ID;
		return $model->findOne()->Situacao;
	}
	
	
}

