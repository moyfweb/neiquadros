<?php
class CarrinhoSession extends CModel {
	
	public $IDSCarrinho = null;
	public $Total = null;
	public $Volumes = null;
	
	public function __construct()
	{
		parent::__construct();
		H::connect();
		$this->setClass(get_class());
		$this->setPK('IDSCarrinho');
		$this->setTable('ecom_carrinho_item');
	}
	
	public function getLabel($key) {
		$labels = array();
		$labels['IDSCarrinho'] = 'Session';
		$labels['Total'] = 'Total';
		$labels['Volumes'] = 'Volumes';
		return $labels[$key];
	}
	
	public function getType($key) {	return false;	}
	

	public static function basicInfoCarrinho($IDSCarrinho){	
		$model = new self();
		if(empty($IDSCarrinho)) return $model;
		$model->IDSCarrinho = $IDSCarrinho;
		return $model->findOne();
	}
	
	public function findOne(){
		$query = sprintf("FROM (
			SELECT 
			c.IDSCarrinho,
			(SELECT SUM(Preco) FROM ecom_carrinho_item as i WHERE i.IDSCarrinho=c.IDSCarrinho AND i.Status=1) AS Total,
			(SELECT COUNT(ID) FROM ecom_carrinho_item as i WHERE i.IDSCarrinho=c.IDSCarrinho AND i.Status=1) AS Volumes
			FROM ecom_carrinho_item as c 
			WHERE IDSCarrinho='%s'
			LIMIT 1) as t",$this->IDSCarrinho);
		$this->setFrom($query);
		return parent::findOne();
	}
	
}